package casual4thyear.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.text.Editable;
import android.widget.Toast;

import casual4thyear.myapplication.truthtablepackage.TruthTableControl;



public class TruthTableGui extends Activity {
    private String[] outputOptions =  {"0,1", "false,true", "F,T"};;
    private String computeCmd = "Compute Truth Table";
    private int currentOutputOption = 0;
    private EditText editText;
    private Button boolExpr;
    private TableRow tableRow;
    private RadioGroup radioGroup;
    private RadioButton zeroOrOne;
    private RadioButton falseOrTrue;
    private RadioButton fOrT;
    private TableLayout tableLayout;
    private TableRow headerRow;
    private LinearLayout mainLay;
    private RadioButton selectedRadioButton;
    private int j;
    private Toast toast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.truth_table_gui_fram);

        mainLay = (LinearLayout) findViewById(R.id.mainLayout);
        editText = (EditText) findViewById(R.id.inputExpression);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkComputeButtonEnable();

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
        boolExpr = (Button) findViewById(R.id.compute);
        boolExpr.setOnClickListener(onClick());
        boolExpr.setEnabled(false);
        tableLayout = (TableLayout) findViewById(R.id.TruthTable_TableModel);
        headerRow = (TableRow) findViewById(R.id.headers);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        ((RadioButton)radioGroup.getChildAt(0)).setChecked(true);
        zeroOrOne = (RadioButton) findViewById(R.id.zeroOrOne);
        falseOrTrue = (RadioButton) findViewById(R.id.falseOrTrue);
        fOrT = (RadioButton) findViewById(R.id.fOrT);

       radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(RadioGroup radioGroup, int i) {

               int selectedId = radioGroup.getCheckedRadioButtonId();
               selectedRadioButton = (RadioButton) findViewById(selectedId);
               for(j=0;j<outputOptions.length;j++) {
                   if (selectedRadioButton.getText().toString().equals(outputOptions[j])) {
                       break;
                   }
               }
               currentOutputOption = j;
                checkComputeButtonEnable();

           }

       });
    }


    public View.OnClickListener onClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tableLayout.removeAllViews();
                headerRow.removeAllViews();

                int q;
                String p[][];
                TruthTableControl t;
                TextView newView;
                TableRow newRow;
               try {
                   t = new TruthTableControl(editText.getText().toString());
                   p = t.getFormattedTruthTable(currentOutputOption);
               } catch (Exception e){
                  Context context = getApplicationContext();
                  CharSequence text = e.getMessage().toString();


                   toast = Toast.makeText(context,text,Toast.LENGTH_SHORT);
                   toast.show();
                    return;

               }

                   for (q = 0; q<t.columnHeaders().length; q++){
                        newView = addProductTerm(t.columnHeaders()[q]);
                       headerRow.addView(newView, q);



                    }
                tableLayout.addView(headerRow);

                for(int row=0; row<p.length;row++){
                       newRow = createRow();
                       newRow.setOrientation(LinearLayout.HORIZONTAL);
                       for(int col=0; col<p[row].length;col++){
                           newView = addProductTerm(p[row][col]);
                            newRow.addView(newView,col);

                       }

                       tableLayout.addView(newRow,row + 1);



                   }


               boolExpr.setEnabled(false);
            }
        };
    }


    private void checkComputeButtonEnable() {
        if (editText.getText().length()==0)
            boolExpr.setEnabled(false);
        else
            boolExpr.setEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_guiframe, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public TextView addProductTerm(String string) {
        final TextView textView = new TextView(this);
        textView.setPadding(25,0,0,0);
        textView.setText(string);

        return textView;
    }

    public TableRow createRow() {
        final TableRow tableRow = new TableRow(this);

        return tableRow;
    }
}
