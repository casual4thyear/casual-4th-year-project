package casual4thyear.myapplication.variablenegationpackage;


import android.app.ActionBar;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class VariableNegation_TableModel extends TableLayout {
    private VariableNegationControl control;
    private String[] outputValues;
    private Object[][] dados;
    private Context appContext;
    private CheckBox checkBox;
    public static final ActionBar.LayoutParams DEFAULT_PARAMS = new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    public VariableNegation_TableModel(VariableNegationControl c,Context appContext)  {
        super(appContext);
        control = c;
        control.prepareArrayResult();
        this.appContext = appContext;
        createdados();

    }

    public int getColumnCount() {
        return (control.getNumberOfColumnsInArrayResult()+2);

    }
    public int getRowCount() {
        return control.getNumberOfLinesInArrayResult();
    }
    public String getColumnName(int col) {
        return control.getVariableName(col);
    }

    public void createdados(){
        TextView newView;
        TableRow newRow;
        for (int row = 0; row < getRowCount(); row++) {
            newRow = new TableRow(appContext);
            this.addView(newRow);
            for (int col = 0; col < getColumnCount(); col++) {
                if (col==0){
                    newView = new TextView(appContext);
                    newView.setText(control.getCellValue(row, col).toString());
                    newRow.addView(newView);
                }else{
                    if (col==getColumnCount()-1) {
                        checkBox = new CheckBox(appContext);
                        checkBox.setChecked(false);
                        newRow.addView(checkBox);
                    }
                    else{
                        newView = new TextView(appContext);
                        String xorNot = (Integer.parseInt(control.getCellValue(row, col))==1) ? "X" : "";
                        newView.setText(xorNot);
                        newRow.addView(newView);

                    }
                }
            }
        }
    }



    public Object getValueAt(int row, int col) {
        TableRow tempRow = (TableRow)this.getChildAt(row);
        View tempRowCol = tempRow.getChildAt(col);

        return tempRowCol;
    }

//    public Object getValueAt(int row, int col) {
//        String s = control.getCellValue(row, col);
//        if (col==0)
//            return s;
//        if (col==getColumnCount()-1)
//            return Boolean.FALSE;
//        else
//            return (Integer.parseInt(s)==1) ? "X" : "";
//    }




    public Class getColumnClass(int col) {
        Class c = null;
        try {
            if (col==getColumnCount()-1)
                c = Class.forName("java.lang.Boolean");
            else
                c = Class.forName("java.lang.String");
        } catch (java.lang.ClassNotFoundException e) {
            System.err.println("ClassNotFooundException when looking for the Class of String or Boolean!");
        }
        return c;
    }


    public boolean isCellEditable(int row, int col) {
        if (col==(getColumnCount()-1))
            return true;
        else
            return false;
    }

    public void setValueAt(Object value, int row, int col) {
        if(col==getColumnCount()-1){
            dados[row][col]=(Boolean) value;
        }


    }

}
