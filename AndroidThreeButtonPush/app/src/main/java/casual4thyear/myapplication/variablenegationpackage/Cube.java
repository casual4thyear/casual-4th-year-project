package casual4thyear.myapplication.variablenegationpackage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

import casual4thyear.myapplication.truthtablepackage.TruthTableControl;

/**
 * Created by Matthew on 2015-11-30.
 */
public class Cube implements Serializable{
    public int[] theCube;
    public ArrayList varNames;

    public Cube(TruthTableControl ttc, ArrayList allVarNames) {
        int i, j, variant;
        boolean found;
        int[] fValues;
        String[] thisTermVarNames;

        varNames = new ArrayList(allVarNames);
        //Initializing the Cube representation
        theCube = new int[allVarNames.size()];
        for(i=0; i<theCube.length; i++)
            theCube[i] = -1;

        //Find the variant that makes the term true
        fValues = ttc.getFunctionValues();
        for(i=0; i<fValues.length; i++) {
            if (fValues[i]==1)
                break;
        }
        variant = i; //i is the variant

        //The variable names in this term
        thisTermVarNames = ttc.variableNames();

        for(i=0; i<allVarNames.size(); i++) {
            //Find the ith variable name in this term variable names
            found = false;
            for(j=0; j<thisTermVarNames.length; j++) {
                if (allVarNames.get(i).equals(thisTermVarNames[j])) {
                    found = true;
                    break;
                }
            }
            if (found) {
                //The value of the cube for the ith variable in allVarNames
                // (i.e., at index i, is the value of the jth variable in
                // the variant
                theCube[i] = (variant>>>(thisTermVarNames.length-j-1))&1;
            } else {
                //The value of the cube for the ith variable in allVarNames
                // (i.e., at index i, is undefined (i.e., -1)
                theCube[i] = -1;
            }
        }

        //System.out.println("Cube: "+this);
    }



    protected Cube(int[] c, ArrayList names) {
        theCube = new int[c.length];
        System.arraycopy(c,0,theCube,0,c.length);
        varNames = new ArrayList(names);
    }

    public Enumeration computeCubeVariants() {
        Vector v;
        int nb, nbVar, i, j, mask=1;
        int[] variant;

        v = new Vector();
        nb = theCube.length;
        nbVar = (int)Math.pow(2, nb);
        variant = new int[nb];
        for(i=0; i<nbVar; i++) {
            for(j=0; j<nb; j++)
                variant[j] = (i>>>(nb-j-1))&mask;
            if (evaluateCube(variant))
                v.add(new Cube(variant, varNames));
        }
        return v.elements();
    }

    public boolean evaluateCube(int[] otherCube) {
        boolean result;

        if (otherCube.length != theCube.length) {
            System.err.print("Different cube lengths in evaluateCube: ");
            System.err.println(otherCube.length+" != "+theCube.length);
            return false;
        }
        result = true;
        for(int i=0; i<theCube.length; i++) {
            if (theCube[i]==-1)
                continue;
            if (theCube[i]!=otherCube[i]) {
                result=false;
                break;
            }
        }
        return result;
    }

    public String toString() {
        String ret="";
        for(int k=0; k<theCube.length; k++)
            ret+=theCube[k]+" ";
        return ret;
    }

    public int intValue() {
        int ret, i, mask=1;
        for(i=0, ret=0; i<theCube.length; i++)
            ret += (int)Math.pow(2, theCube.length-i-1) * theCube[i];
        return ret;
    }

    public String stringValue() {
        String ret = "";
        for(int k=0; k<theCube.length; k++) {
            if (theCube[k]==-1)
                continue;
            if (theCube[k]==1)
                ret+=varNames.get(k)+" and ";
            else
                ret+="not("+varNames.get(k)+") and ";
        }
        return ret.substring(0, ret.lastIndexOf(" and"));
    }
}
