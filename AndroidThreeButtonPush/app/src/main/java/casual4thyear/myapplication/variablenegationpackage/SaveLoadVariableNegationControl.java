package casual4thyear.myapplication.variablenegationpackage;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Zacharie on 2015-12-02.
 */
public class SaveLoadVariableNegationControl {
    public static final String VARIABLE_NEGATION_CONTROL_SER = "VariableNegationControl.ser";
    public static void saveVariableNegationControl(VariableNegationControl c, Context context) throws IOException {
        FileOutputStream fos = context.openFileOutput(VARIABLE_NEGATION_CONTROL_SER, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(c);
        os.close();
        fos.close();
    }
    public static  VariableNegationControl loadVariableNegationControl(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(VARIABLE_NEGATION_CONTROL_SER);
        ObjectInputStream is = new ObjectInputStream(fis);
        VariableNegationControl vnc = (VariableNegationControl) is.readObject();
        is.close();
        fis.close();
        return vnc;
    }
}
