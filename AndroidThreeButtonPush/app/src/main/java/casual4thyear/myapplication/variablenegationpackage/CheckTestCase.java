package casual4thyear.myapplication.variablenegationpackage;

import android.widget.CheckBox;
import android.widget.TextView;

public class CheckTestCase {	//checks if the test case chosen is good (covers all cases)
    int lines;
    int col;
    int numberOfCases;
    Boolean[] selectedTest;
    Boolean[] myTestCase;


    public CheckTestCase(VariableNegation_TableModel tableModel){
        lines = tableModel.getRowCount();
        col = tableModel.getColumnCount();
        numberOfCases = col - 3;
        selectedTest  = new Boolean[lines];
        myTestCase = new Boolean[numberOfCases];

        //myTestCase filled with false
        for (int i = 0; i < numberOfCases; i++) {
            myTestCase[i]=Boolean.FALSE;
        }

        for (int j = 0; j <lines; j++) {
            selectedTest[j] = ((CheckBox) tableModel.getValueAt(j, col-1)).isChecked();
            if (selectedTest[j]){
                for (int i = 0; i < numberOfCases; i++) {
                    if(((TextView)tableModel.getValueAt(j, i+1)).getText()=="X"){
                        myTestCase[i]=Boolean.TRUE;
                    }
                }
            }
        }

    }

    public Boolean isGoodTest(){
        for (int i = 0; i < numberOfCases; i++) {
            if(!myTestCase[i]){	//if there's one case that was not assigned, then it's not a good choice of test case
                return false;
            }
        }
        return true;
    }

}
