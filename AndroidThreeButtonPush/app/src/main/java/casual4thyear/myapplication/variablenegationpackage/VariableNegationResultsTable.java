package casual4thyear.myapplication.variablenegationpackage;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class VariableNegationResultsTable extends Activity {


    public static final String IS_MY_TEST_GOOD = "Is my test good?";

    // window opens when showTestCaseButton is pressed
    public static final ActionBar.LayoutParams DEFAULT_PARAMS = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    public static final ActionBar.LayoutParams DEFAULT_MAIN_PARAMS = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    public static final ActionBar.LayoutParams DEFAULT_PARAMS_LL3 = new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    public static final String ANSWER_HERE = "Answer here";
    public static final boolean ENABLED = true;
    public static final boolean DISABLED = false;
    public static final int VIEW_INIT_ID = 10;

    private VariableNegationControl control;
    private VariableNegation_TableModel tableModel;
    private RelativeLayout mainLayout;

    //    JPanel jp1 = new JPanel();
    private LinearLayout ll_1;
    private ScrollView scrollViewLL1;
//    JPanel jp2 = new JPanel();
    private LinearLayout ll_2;
    private ScrollView scrollViewLL2;
//    JPanel jp3 = new JPanel();
    private LinearLayout ll_3;
    private ScrollView scrollViewForRTable;
//    JButton mytestcaseButton = new JButton("Is my test good?");
    private Button myTestCaseButton;
//    JLabel answerLabel = new JLabel("Answer Here");
    private TextView answerLabel;
    int p, k;
    private RelativeLayout.LayoutParams relative_param_default_top;
    private RelativeLayout.LayoutParams relative_param_default_bot;
    private Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mainLayout = new RelativeLayout(getApplicationContext());
        mainLayout.setLayoutParams(DEFAULT_MAIN_PARAMS);
        setContentView(mainLayout);
        try {
            VariableNegationControl cont = SaveLoadVariableNegationControl.loadVariableNegationControl(getApplicationContext());
            loadWidgets(cont);
        }catch(Exception e){
            Context context = getApplicationContext();
            CharSequence text = e.getMessage().toString();
            toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
            toast.show();
            return;
        }

    }
    //	public VariableNegationResultsTable(String s) { //this part is used to run only this window
    public VariableNegationResultsTable() { //comment this to run only this window

    }
    public void loadWidgets(VariableNegationControl c){
        control=c;		//comment this to run only this window
        setUpLayoutParams();
        control.prepareArrayResult();

        initiateLLParams();

        createCaptionPanels();
        tableModel = new VariableNegation_TableModel(control,getApplicationContext());

        TableLayout resultTable = tableModel;
        scrollViewForRTable.addView(resultTable);

        myTestCaseButton = new Button(getApplicationContext());
        myTestCaseButton.setText(IS_MY_TEST_GOOD);
        ll_3.addView(myTestCaseButton,DEFAULT_PARAMS_LL3);

        answerLabel = new TextView(getApplicationContext());
        answerLabel.setEnabled(ENABLED);
        answerLabel.setText(ANSWER_HERE);
        ll_3.addView(answerLabel,DEFAULT_PARAMS_LL3);

        myTestCaseButton.setOnClickListener(isMyTestGood());

    }
    private View.OnClickListener isMyTestGood() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckTestCase check;
                int lastColumn = tableModel.getColumnCount() - 1;
                int lines = tableModel.getRowCount();
                Boolean[] selectedTestCase = new Boolean[lines];
                for (int j = 0; j < lines; j++) {
                    selectedTestCase[j] = ((CheckBox) tableModel.getValueAt(j, lastColumn)).isChecked();
                    check = new CheckTestCase(tableModel);
                    if (check.isGoodTest()) {
                        answerLabel.setText("That's a good choice!");
                    } else {
                        answerLabel.setText("Not really. Try again.");
                    }
                }
            }
        };
    }
    private void initiateLLParams() {
        int viewId = VIEW_INIT_ID;
        ll_1 = new LinearLayout(getApplicationContext());
        ll_1.setOrientation(LinearLayout.VERTICAL);
        ll_1.setId(viewId++);
        scrollViewLL1 = new ScrollView(getApplicationContext());
        scrollViewLL1.setId(viewId++);

        scrollViewLL1.addView(ll_1);

        ll_2 = new LinearLayout(getApplicationContext());
        ll_2.setOrientation(LinearLayout.VERTICAL);
        ll_2.setId(viewId++);
        scrollViewLL2 = new ScrollView(getApplicationContext());
        scrollViewLL2.setId(viewId++);

        scrollViewLL2.addView(ll_2);

        scrollViewForRTable = new ScrollView(getApplicationContext());
        scrollViewForRTable.setId(viewId++);

        ll_3 = new LinearLayout(getApplicationContext());
        ll_3.setOrientation(LinearLayout.HORIZONTAL);
        ll_3.setGravity(View.TEXT_ALIGNMENT_CENTER);
        ll_3.setId(viewId++);

        mainLayout.addView(scrollViewLL1, relative_param_default_top);
        mainLayout.addView(scrollViewLL2,alignRelativeView(scrollViewLL1));
        mainLayout.addView(scrollViewForRTable,alignRelativeView(scrollViewLL2,ll_3));
        mainLayout.addView(ll_3,relative_param_default_bot);
    }

    public void createCaptionPanels() {
        ArrayList captions = control.getLabelsCaption();
        TextView temp;

        for (k = 0; k < captions.size(); k++) {
            temp = new TextView(getApplicationContext());
            temp.setText(captions.get(k).toString());
            if (k < captions.size() / 2)
                ll_1.addView(temp);
            else
                ll_2.addView(temp);

        }
    }


    public RelativeLayout.LayoutParams alignRelativeView(View topView){
        RelativeLayout.LayoutParams rel_lay = new RelativeLayout.LayoutParams(DEFAULT_PARAMS);
        rel_lay.addRule(RelativeLayout.BELOW, topView.getId());
        return rel_lay;
    }
    public RelativeLayout.LayoutParams alignRelativeView(View topView, View botView){
        RelativeLayout.LayoutParams rel_lay = alignRelativeView(topView);
        rel_lay.addRule(RelativeLayout.ABOVE,botView.getId());
        return rel_lay;
    }
    public void setUpLayoutParams(){
        relative_param_default_top = new RelativeLayout.LayoutParams(DEFAULT_PARAMS);
        relative_param_default_top.addRule(RelativeLayout.ALIGN_PARENT_TOP);

        relative_param_default_bot = new RelativeLayout.LayoutParams(DEFAULT_PARAMS);
        relative_param_default_bot.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
    }
    ////////////////		this part is used to run only this window
//	private void callVNC(){
//		Vector productTerms = new Vector(2,5);
//		productTerms.add("a and b and not c");
//		productTerms.add("a and d");
//		try {
//			control = new VariableNegationControl(productTerms.elements());
//
//		}	catch (Exception e) {
//		 	System.out.println(e);
//			}
//	}
//
//
//    public static void main(String[] args) {
//        javax.swing.SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//                VariableNegationResultsTable gui = new VariableNegationResultsTable("HOLA");
//            }
//        });
//    }
    /////////////
}