package casual4thyear.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.widget.Button;

import casual4thyear.myapplication.dnfexpressionpackage.DNFControl;
import casual4thyear.myapplication.dnfexpressionpackage.DNFExeFrame;

public class MainGUIFrame extends Activity {
    private Button truthTable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_guiframe);
        truthTable = (Button) findViewById(R.id.truthTableButton);
        truthTable.setEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_guiframe, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void truthTables(View view){
        Intent intent = new Intent(MainGUIFrame.this, TruthTableGui.class);
        startActivity(intent);
    }

    public void variableNegation(View view){
        Intent intent = new Intent(MainGUIFrame.this, VariableNegation.class);
        startActivity(intent);
    }

    public void dnfExpression(View view){
        Intent intent = new Intent(MainGUIFrame.this, DNFExeFrame.class);
        startActivity(intent);
    }
}
