package casual4thyear.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Enumeration;
import java.util.Vector;

import casual4thyear.myapplication.variablenegationpackage.SaveLoadVariableNegationControl;
import casual4thyear.myapplication.variablenegationpackage.VariableNegationControl;


public class VariableNegation extends Activity {

    public static final int EXTRA_CHILD_NUMBER = 2;
    private Button addProdTerm;
    private Button computeButton;
    private LinearLayout mLayout;
    private EditText firstProductTerm;
    private Vector listOfJTextField;
    VariableNegationControl control;
    private Toast toast;


    private int itemindex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.variable_negation);
        mLayout = (LinearLayout) findViewById(R.id.varNegation);
        computeButton = (Button) findViewById(R.id.computeVarNeg);
        computeButton.setEnabled(false);
        addProdTerm = (Button) findViewById(R.id.addProdTerm);
        firstProductTerm = (EditText) findViewById(R.id.editText);
        makeListener(firstProductTerm);
        listOfJTextField = new Vector();
        listOfJTextField.add(firstProductTerm);



    }

    public OnClickListener addProdTerm() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                itemindex = mLayout.getChildCount() - EXTRA_CHILD_NUMBER;

                mLayout.addView(addProductTerm(),itemindex);
                computeButton.setEnabled(false);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.variable_negation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void computeVarNeg(View view) {
        Intent intent = new Intent(VariableNegation.this, ComputedVarNeg.class);

        EditText jtf;
        Vector productTerms = new Vector();
        Enumeration enu = listOfJTextField.elements();
        int it=0;
        while(enu.hasMoreElements()) {
            jtf = (EditText)enu.nextElement();
            if (jtf.getText().length()==0)
                continue;
            productTerms.add(jtf.getText());
            it++;


        }


        try {
            control = new VariableNegationControl(productTerms.elements());
            String message = control.getRawResults();
            intent.putExtra("computedVarNeg", message);
            SaveLoadVariableNegationControl.saveVariableNegationControl(control,getApplicationContext());
            startActivity(intent);
        } catch (Exception e) {
            Context context = getApplicationContext();
            CharSequence text = e.getMessage().toString();


            toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
            toast.show();
            return;
        }


    }

    public EditText addProductTerm() {
        final LayoutParams lparams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        final EditText editText = new EditText(this);
        editText.setLayoutParams(lparams);
        listOfJTextField.add(editText);
        makeListener(editText);
        computeButton.setEnabled(false);
        return editText;
    }

    private void theDocumentListener() {
        Enumeration enu;
        boolean test;
        EditText jtf;

        test = false;
        enu = listOfJTextField.elements();
        while (enu.hasMoreElements()) {
            jtf = (EditText) enu.nextElement();
            if (jtf.getText().length() > 0) {
                test = true;
                break;
            }
        }
        if (!test)
            computeButton.setEnabled(false);
        else
            computeButton.setEnabled(true);
    }

    private void makeListener(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                theDocumentListener();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
    }

}
