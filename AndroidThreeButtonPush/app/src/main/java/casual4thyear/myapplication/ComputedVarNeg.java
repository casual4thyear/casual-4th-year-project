package casual4thyear.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;

import casual4thyear.myapplication.variablenegationpackage.VariableNegationControl;
import casual4thyear.myapplication.variablenegationpackage.VariableNegationResultsTable;


public class ComputedVarNeg extends Activity {

    private TextView varNegResults;
    private VariableNegationControl control;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.computed_var_neg);
        varNegResults = (TextView) findViewById(R.id.varNegResults);
        Intent intent = getIntent();
        String messageText = intent.getExtras().getString("computedVarNeg");
        varNegResults.setText(messageText);
        control = (VariableNegationControl) intent.getSerializableExtra("control");
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.computed_varneg_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showTestCases(View view){
        Intent intent = new Intent(ComputedVarNeg.this, VariableNegationResultsTable.class);
        Bundle conBun = new Bundle();
        conBun.putSerializable("control",control);
        intent.putExtras(conBun);
        startActivity(intent);

    }

}
