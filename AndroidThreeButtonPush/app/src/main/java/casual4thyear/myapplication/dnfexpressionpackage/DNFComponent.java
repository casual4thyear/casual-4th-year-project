package casual4thyear.myapplication.dnfexpressionpackage;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import casual4thyear.myapplication.R;

public class DNFComponent {
    public static final boolean DISABLED = false;
    public static final boolean ENABLED = true;

    public static final int INPUT_TEXT_VIEW_INDEX = 0;
    public static final int INPUT_EDIT_VIEW_INDEX = 1;
    public static final int OUTPUT_TEXT_VIEW_INDEX = 2;
    public static final int SCROLL_VIEW_INDEX = 3;
    public static final int OUTPUT_EDIT_VIEW_INDEX = 0;
    public static final int COMPUTE_BUTTON_VIEW_INDEX = 4;

    public static final ActionBar.LayoutParams DEFAULT_PARAMS = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


    //public static final ActionBar.LayoutParams RELATIVE_PARAMS = new ActionBar.LayoutParams();


    private static final String INPUT_TEXT_STRING = "Enter the boolean function below:";
    private static final String OUTPUT_TEXT_STRING = "Terms in the Disjunctive Normal Form (DNF)";
    private static final String COMPUTE_BUTTON_STRING = "COMPUTE DNF";


    private Activity parentActivity;
    private RelativeLayout mainLayout;
    private TextView inputTextView;
    private EditText inputExpression;
    private TextView outputTextView;
    private ScrollView scrollView;
    private EditText outputExpression;
    private Button computeButton;
    private Toast toast;

    private RelativeLayout.LayoutParams relative_param_default_top;
    private RelativeLayout.LayoutParams relative_param_default_bot;

    private int viewid;

    public DNFComponent(Activity dnfActivity) {
        parentActivity = dnfActivity;
        mainLayout = (RelativeLayout) parentActivity.findViewById(R.id.mainLayout);
        setUpLayoutParams();
        loadWidgets();
    }





    private void loadWidgets() {
        viewid = 10;
        //Text view for expression
        inputTextView = new TextView(parentActivity.getApplicationContext());
        inputTextView.setText(INPUT_TEXT_STRING);
        inputTextView.setId(viewid++);

        //Input expression widget for entering text for dnf
        inputExpression = new EditText(parentActivity.getApplicationContext());
        inputExpression.setEnabled(ENABLED);
        inputExpression.addTextChangedListener(getInputTextWatcher());
        inputExpression.setId(viewid++);
        //Output text view for expression
        outputTextView = new TextView(parentActivity.getApplicationContext());
        outputTextView.setText(OUTPUT_TEXT_STRING);
        outputTextView.setId(viewid++);
        //Scroll view for output expression to nest
        scrollView = new ScrollView(parentActivity.getApplicationContext());
        scrollView.setId(viewid++);
        //output edit text for computed expression
        outputExpression = new EditText(parentActivity.getApplicationContext());
        outputExpression.setEnabled(DISABLED);
        outputExpression.setTextColor(Color.WHITE);
        outputExpression.setId(viewid++);
        //Add output in scrollView
        // button for compute
        computeButton = new Button(parentActivity.getApplicationContext());
        computeButton.setText(COMPUTE_BUTTON_STRING);
        computeButton.setEnabled(DISABLED);
        computeButton.setOnClickListener(computeCMDClick());
        computeButton.setId(viewid++);

        mainLayout.addView(inputTextView, INPUT_TEXT_VIEW_INDEX, relative_param_default_top);
        mainLayout.addView(inputExpression, INPUT_EDIT_VIEW_INDEX, alignRelativeView(inputTextView));
        mainLayout.addView(outputTextView, OUTPUT_TEXT_VIEW_INDEX, alignRelativeView(inputExpression));
        mainLayout.addView(scrollView, SCROLL_VIEW_INDEX, alignRelativeView(outputTextView,computeButton));
        scrollView.addView(outputExpression, OUTPUT_EDIT_VIEW_INDEX, DEFAULT_PARAMS);

        mainLayout.addView(computeButton, COMPUTE_BUTTON_VIEW_INDEX, relative_param_default_bot);


    }
    private TextWatcher getInputTextWatcher(){
        return new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkComputeButtonEnable();

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Left empty because we do not have implementation for beforeTextChanged
            }

            @Override
            public void afterTextChanged(Editable s) {

                //Left empty because we do not have implementation for afterTextChanged
            }
        };
    }

    private View.OnClickListener computeCMDClick(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v)  {
                try{
                    outputExpression.setEnabled(ENABLED);
                    outputExpression.setText("");
                    DNFControl d = new DNFControl(inputExpression.getText().toString());
                    for(int y=0; y<d.size(); y++){
                        outputExpression.append("Term "+(1+y)+": "+d.get(y)+"\n");
                    }
                    outputExpression.setEnabled(DISABLED);
                    computeButton.setEnabled(DISABLED);
                }catch(Exception e){
                    Context context = parentActivity.getApplicationContext();
                    CharSequence text = e.getMessage().toString();
                    toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
            }
        };
    }
    private void checkComputeButtonEnable() {
        if (inputExpression.getText().length()==0)
            computeButton.setEnabled(DISABLED);
        else
            computeButton.setEnabled(ENABLED);
    }
    public void setUpLayoutParams(){
        relative_param_default_top = new RelativeLayout.LayoutParams(DEFAULT_PARAMS);
        relative_param_default_top.addRule(RelativeLayout.ALIGN_PARENT_TOP);

        relative_param_default_bot = new RelativeLayout.LayoutParams(DEFAULT_PARAMS);
        relative_param_default_bot.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
    }
    public RelativeLayout.LayoutParams alignRelativeView(View topView){
        RelativeLayout.LayoutParams rel_lay = new RelativeLayout.LayoutParams(DEFAULT_PARAMS);
        rel_lay.addRule(RelativeLayout.BELOW, topView.getId());
        return rel_lay;
    }
    public RelativeLayout.LayoutParams alignRelativeView(View topView, View botView){
        RelativeLayout.LayoutParams rel_lay = alignRelativeView(topView);
        rel_lay.addRule(RelativeLayout.ABOVE,botView.getId());
        return rel_lay;
    }

}
