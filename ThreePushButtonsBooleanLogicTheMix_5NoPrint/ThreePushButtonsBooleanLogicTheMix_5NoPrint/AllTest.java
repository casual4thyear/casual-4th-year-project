

import junit.framework.*;

public class AllTest {
	public static Test suite(){
		
		
		
		TestSuite suite = new TestSuite();      
		
		
		suite.addTest(new TruthTableTest("testTruthTable"));
		suite.addTest(new TruthTableTest("testVariableNamesTTC"));
		suite.addTest(new TruthTableTest("testColumnHeadersTTC"));
		suite.addTest(new TruthTableTest("testGetFormattedTruthTableTTC1"));
		suite.addTest(new TruthTableTest("testGetFormattedTruthTableTTC2"));
		suite.addTest(new TruthTableTest("testGetFormattedTruthTableTTC"));
		
		suite.addTest(new DNFControlTest("testSize"));
		suite.addTest(new DNFControlTest("testGet1"));
		suite.addTest(new DNFControlTest("testGet2"));
		
		suite.addTest(new VariableNegationControlTest("testVariableNegation1"));	
		suite.addTest(new VariableNegationControlTest("testVariableNegation2"));
		suite.addTest(new VariableNegationControlTest("testPrepareArrayResult"));
		
		
		
		return suite;
		
	}

}
