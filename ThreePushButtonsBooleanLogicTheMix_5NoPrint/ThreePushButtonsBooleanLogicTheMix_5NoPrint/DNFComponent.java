

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;



import java.util.Enumeration;

public class DNFComponent implements ActionListener, DocumentListener {
    private JPanel mainPane;
    private JFrame parentFrame;
    private JTextField inputExpression;
    private JButton computeButton;
    private JTextArea outputExpression;
    private JPanel outputPanel;
    private String computeCmd = "Compute DNF";

    public DNFComponent() {
        mainPane = new JPanel();
        addWidgets();
        mainPane.setVisible(true);
    }

    public JComponent getComponent() {
        return mainPane;
    }

    public void setParentFrame(JFrame f) {
        parentFrame = f;
        parentFrame.getRootPane().setDefaultButton(computeButton);
    }

    private void addWidgets() {
        //Create the input and output text area
        JPanel textInputPanel = new JPanel();
        textInputPanel.setLayout(new BoxLayout(textInputPanel,BoxLayout.X_AXIS));
        textInputPanel.setBorder(BorderFactory.createTitledBorder("Enter the boolean function below:"));
        inputExpression = new JTextField(20);
        inputExpression.setMaximumSize(new Dimension(300,25));
        inputExpression.getDocument().addDocumentListener(this);
        textInputPanel.add(inputExpression);
        textInputPanel.setMaximumSize(new Dimension(305,50));
        textInputPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        JPanel textOutputPanel = new JPanel();
        textOutputPanel.setLayout(new BoxLayout(textOutputPanel,BoxLayout.X_AXIS));
        textOutputPanel.setBorder(BorderFactory.createTitledBorder("Terms in the Disjunctive Normal Form (DND)"));
        outputExpression = new JTextArea();
        outputExpression.setMaximumSize(new Dimension(300,200));
        textOutputPanel.add(new JScrollPane(outputExpression));
        textOutputPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        //Create a panel with these input elements: lPane
        JPanel lPane = new JPanel();
        lPane.setLayout(new BoxLayout(lPane, BoxLayout.Y_AXIS));
        lPane.add(textInputPanel);
        lPane.add(textOutputPanel);

        //Create the "Compute" button
        computeButton = new JButton(computeCmd);
        computeButton.addActionListener(this);
        computeButton.setEnabled(false);
        computeButton.setActionCommand(computeCmd);

        //Create an panel with all elements: inputPane
        JPanel inputPane = new JPanel();
        inputPane.setLayout(new BoxLayout(inputPane, BoxLayout.X_AXIS));
        inputPane.add(lPane);
        inputPane.add(computeButton);

        //Create an output panel to store the result
        outputPanel = new JPanel();
        outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.Y_AXIS));

        //Add everything to the mainPane
        //mainPane = new JPanel();
        mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
        mainPane.setBorder(BorderFactory.createEmptyBorder(
                                        20, //top
                                        50, //left
                                        20, //bottom
                                        50) //right
                                        );
        mainPane.add(inputPane);
        mainPane.add(outputPanel);
        //return mainPane;
    }

    public void actionPerformed(ActionEvent evt) {
        if (evt.getActionCommand().equals(computeCmd)) {
           /*
            * The following two lines of code were added to track execution
            * August-4-2015
            */
         	try {
            	outputExpression.setText("");
                DNFControl d = new DNFControl(inputExpression.getText());
                 for(int y=0; y<d.size(); y++){
                	 outputExpression.append("Term "+(1+y)+": "+d.get(y)+"\n");
                }
                parentFrame.pack();
                parentFrame.validate();
                computeButton.setEnabled(false); //diseable the compute button
            } catch (Exception e) {
                JOptionPane.showMessageDialog(parentFrame, e.getMessage(), "Parsing Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            System.err.println("ActionEvent unknown in DNFComponent: "+evt);
        }
    }

    // listeners to detect changes in the input text field and enable the compute button
    public void insertUpdate(DocumentEvent e) {
        theDocumentListener(e);
    }
    public void removeUpdate(DocumentEvent e) {
        theDocumentListener(e);
    }
    public void changedUpdate(DocumentEvent e) {
        theDocumentListener(e);
    }
    private void theDocumentListener(DocumentEvent e) {
        if (inputExpression.getText().length()==0)
            computeButton.setEnabled(false);
        else
            computeButton.setEnabled(true);
    }

}
