


import junit.framework.*;



public class TruthTableTest extends TestCase {
	

	TruthTableControl tt;
				
	public TruthTableTest(String a){
		super(a);
	}	
		
	
	private void createTruthTable(String logExp){
		try {
			tt = new TruthTableControl(logExp);			
			
		}	catch (Exception e) {
		 	System.out.println(e);
			}
	}
	
	

	
	public void testTruthTable() {
		int expResult[]= {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //array - results expected of the truth table for each row		
		String LogicalExpression;

////////////////////////////////////////////////////////////////////////////////////////
		
/*		 						TEST 1
		   ** "x AND y OR Z"**
		   *numberOfVariables=3 
		   *expected result: [0 1 0 1 0 1 1 1]	*/

		LogicalExpression="x  AND (y OR Z)";
		
		expResult[0]=0;
		expResult[1]=0;
		expResult[2]=0;
		expResult[3]=0;
		expResult[4]=0;
		expResult[5]=1;
		expResult[6]=1;
		expResult[7]=1;
		
		createTruthTable(LogicalExpression);
		startTest(3, LogicalExpression, expResult);
			
		
		
////////////////////////////////////////////////////////////////////////////////////////		
//		 						TEST 2
//		   ** a and (b or F)**
//		   *numberOfVariables=3 
//		   *expected result: [0 1 0 1 0 1 1 1]	
		
		
		LogicalExpression="a and (b or F)";
		expResult[0]=0;
		expResult[1]=0;
		expResult[2]=0;
		expResult[3]=0;
		expResult[4]=0;
		expResult[5]=1;
		expResult[6]=1;
		expResult[7]=1;
		
		createTruthTable(LogicalExpression);
		startTest(3, LogicalExpression, expResult);

		
////////////////////////////////////////////////////////////////////////////////////////		
//		 						TEST 4
//		   ** a xor b**
//		   *numberOfVariables=2 
//		   *expected result: [0 1 1 0]	
		
		
		LogicalExpression="a xor b";
		expResult[0]=0;
		expResult[1]=1;
		expResult[2]=1;
		expResult[3]=0;

		createTruthTable(LogicalExpression);
		startTest(2, LogicalExpression, expResult);
		
		
//////////////////////////////////////////////////////////////////////////////////////////////////		
//				 						TEST 5
//		   ** a or not b**
//		   *numberOfVariables=2 
//		   *expected result: [1 0 1 1]	
		
		LogicalExpression="a or (not b)";
		expResult[0]=1;
		expResult[1]=0;
		expResult[2]=1;
		expResult[3]=1;

		createTruthTable(LogicalExpression);
		startTest(2, LogicalExpression, expResult);
		
		

//////////////////////////////////////////////////////////////////////////////////////////////////
//				 						TEST 6
//		   ** a xnor (b nand    c)**
//		   *numberOfVariables=3 
//		   *expected result: [0 0 0 1 1 1 1 0]	
		
		LogicalExpression="a XNOR (b nand    R)";
		expResult[0]=0;
		expResult[1]=0;
		expResult[2]=0;
		expResult[3]=1;
		expResult[4]=1;
		expResult[5]=1;
		expResult[6]=1;
		expResult[7]=0;
		
		createTruthTable(LogicalExpression);
		startTest(3, LogicalExpression, expResult);

		
//////////////////////////////////////////////////////////////////////////////////////////////////
//				 						TEST 7
//		   ** a NOR b**
//		   *numberOfVariables=2 
//		   *expected result: [1 0 0 0]	
		
		LogicalExpression="a NOR b";
		expResult[0]=1;
		expResult[1]=0;
		expResult[2]=0;
		expResult[3]=0;

		createTruthTable(LogicalExpression);
		startTest(2, LogicalExpression, expResult);
		
		

		
//////////////////////////////////////////////////////////////////////////////////////////////////
//						 						TEST 8
//		   ** (A AND d) XOR (NOT (C XNOR (NOT b)))**
//		   *numberOfVariables=4 
//		   *expected result: [0 1 1 0 0 1 1 0 0 1 1 0 1 0 0 1]	
		
		LogicalExpression="(A AND d) XOR (not(C XNOR (NOT b)))";
		expResult[0]=1;
		expResult[1]=0;
		expResult[2]=0;
		expResult[3]=1;
		expResult[4]=1;
		expResult[5]=0;
		expResult[6]=0;
		expResult[7]=1;
		expResult[8]=1;
		expResult[9]=0;
		expResult[10]=0;
		expResult[11]=1;
		expResult[12]=0;
		expResult[13]=1;
		expResult[14]=1;
		expResult[15]=0;
		
		createTruthTable(LogicalExpression);
		startTest(4, LogicalExpression, expResult);	
		
		
//////////////////////////////////////////////////////////////////////////////////////////////////
//						 						TEST 9
//				   ** a an b**
//				   *numberOfVariables=2 
//				   *expected result: [1 0 0 0]	
				
				LogicalExpression="\r a \n and \t b";
				expResult[0]=0;
				expResult[1]=0;
				expResult[2]=0;
				expResult[3]=1;


				createTruthTable(LogicalExpression);
				startTest(2, LogicalExpression, expResult);		
		
		
		
		
/////*********************************************************************************************/////		
	}
	
	private void testGetTruthTableCellValueTTC(int r, int c, String logicalExp, int exR){
		// r = row of the T.Table
		// c - column of the T.Table
		// ex - expected result	
		int actResult = tt.getTruthTableCellValue(r, c);
		assertSame("*" + logicalExp+ " *" + ", for row= "+ r +", result should be " + exR + " but it was " + actResult, exR, actResult);
	}
	
	private void startTest (int numberOfVariables, String LogExp, int expRes[]) {
		int numberOfRows;
		numberOfRows=(int) Math.pow(2,numberOfVariables);
		for(int i=0; i<numberOfRows; i++){
			testGetTruthTableCellValueTTC(i, numberOfVariables, LogExp, expRes[i]);			
		}
	}

	public void testVariableNamesTTC(){
		int variableNumber=0;

		String LogicalExpression= "a and b AND C";
		String expResult[]={"","",""};
		String aResult[]=null;
		expResult[0]="a"; 
		expResult[1]="b"; 
		expResult[2]="C"; 
		
		createTruthTable(LogicalExpression);
		aResult=tt.variableNames();
		variableNumber=tt.getVariableNumber();
		assertEquals(3, variableNumber);
		for(int i=0; i<variableNumber; i++)
			assertEquals(aResult[i],expResult[i]);
		}

	public void testColumnHeadersTTC(){
		 String headers[];
		 String LogicalExpression="a and B";
		 createTruthTable(LogicalExpression);
		 headers=tt.columnHeaders();
		 assertEquals(2, tt.getVariableNumber());
		 assertEquals("a", headers[0]);
		 assertEquals("B", headers[1]);
		 assertEquals("a and B", headers[2]);
	}

	public void testGetFormattedTruthTableTTC1(){
		String formatedTable[][]={{"false","false","false"},{"false","false","false"},{"false","false","false"},{"false","false","false"}};
		String aResult[][];
		String LogicalExpression="a or B";
		createTruthTable(LogicalExpression);
		formatedTable[0][0]="false";//00
		formatedTable[0][1]="false";
		formatedTable[0][2]="false";
		formatedTable[1][0]="false";//01
		formatedTable[1][1]="true";
		formatedTable[1][2]="true";		
		formatedTable[2][0]="true";//10
		formatedTable[2][1]="false";
		formatedTable[2][2]="true";
		formatedTable[3][0]="true";//11
		formatedTable[3][1]="true";
		formatedTable[3][2]="true";
		aResult=tt.getFormattedTruthTable(1);
	//	System.out.println(tt.getFormattedTruthTable(1));
		for(int i=0;i<=tt.getVariableNumber();i++){
			for(int j=0;j<tt.getVariableNumber();j++){
				//System.out.println(actResult[i][j]);
				assertEquals(formatedTable[i][j],aResult[i][j]);
			}
		}
	}

	public void testGetFormattedTruthTableTTC2(){
		String formatedTable[][]={{"F","F","F"},{"F","F","F"},{"F","F","F"},{"F","F","F"}};
		String aResult[][];
		String LogicalExpression="a and B";
		createTruthTable(LogicalExpression);
		formatedTable[0][0]="F";//00
		formatedTable[0][1]="F";
		formatedTable[0][2]="F";
		formatedTable[1][0]="F";//01
		formatedTable[1][1]="T";
		formatedTable[1][2]="F";		
		formatedTable[2][0]="T";//10
		formatedTable[2][1]="F";
		formatedTable[2][2]="F";
		formatedTable[3][0]="T";//11
		formatedTable[3][1]="T";
		formatedTable[3][2]="T";
		aResult=tt.getFormattedTruthTable(2);
	//	System.out.println(tt.getFormattedTruthTable(1));
		for(int i=0;i<=tt.getVariableNumber();i++){
			for(int j=0;j<tt.getVariableNumber();j++){
				//System.out.println(actResult[i][j]);
				assertEquals(formatedTable[i][j],aResult[i][j]);
			}
		}
	}
		
	public void testGetFormattedTruthTableTTC(){
		String formatedTable[][]={{"0","0","0"},{"0","0","0"},{"0","0","0"},{"0","0","0"}};
		String aResult[][];
		String LogicalExpression="a and B";
		createTruthTable(LogicalExpression);
		formatedTable[0][0]="0";//00
		formatedTable[0][1]="0";
		formatedTable[0][2]="0";
		formatedTable[1][0]="0";//01
		formatedTable[1][1]="1";
		formatedTable[1][2]="0";		
		formatedTable[2][0]="1";//10
		formatedTable[2][1]="0";
		formatedTable[2][2]="0";
		formatedTable[3][0]="1";//11
		formatedTable[3][1]="1";
		formatedTable[3][2]="1";
		aResult=tt.getFormattedTruthTable(0);
	//	System.out.println(tt.getFormattedTruthTable(1));
		for(int i=0;i<=tt.getVariableNumber();i++){
			for(int j=0;j<tt.getVariableNumber();j++){
				//System.out.println(actResult[i][j]);
				assertEquals(formatedTable[i][j],aResult[i][j]);
			}
		}
	}
		
}
