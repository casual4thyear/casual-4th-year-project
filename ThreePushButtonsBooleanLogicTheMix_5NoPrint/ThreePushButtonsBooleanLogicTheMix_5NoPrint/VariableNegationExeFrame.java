

import javax.swing.*;
import javax.swing.event.*;

public class VariableNegationExeFrame extends JFrame{
    private JPanel variableNegationPanel;
    public VariableNegationExeFrame() {
        super("Variable Negation Exe Frame");
        variableNegationPanel = new JPanel();
        VariableNegationComponent v = new VariableNegationComponent();
        v.setParentFrame(this);
        variableNegationPanel.add("Variable Negation", v.getComponent());
        getContentPane().add(variableNegationPanel);
    }
}

