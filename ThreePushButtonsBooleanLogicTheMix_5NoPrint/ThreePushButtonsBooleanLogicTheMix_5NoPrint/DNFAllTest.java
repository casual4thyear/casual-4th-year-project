

import junit.framework.*;

public class DNFAllTest {
	public static Test suite(){
		TestSuite suite = new TestSuite();    
		suite.addTest(new DNFControlTest("testSize"));
		suite.addTest(new DNFControlTest("testGet1"));
		suite.addTest(new DNFControlTest("testGet2"));
		return suite;
		
	}

}
