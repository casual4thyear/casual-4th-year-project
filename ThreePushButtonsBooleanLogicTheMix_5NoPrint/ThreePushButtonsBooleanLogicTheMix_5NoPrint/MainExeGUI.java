 

import javax.swing.JFrame;
import javax.swing.UIManager;



public class MainExeGUI {

    private static void initLookAndFeel() {
        String lookAndFeel = null;
        // For the Java look and feel: getCrossPlatformLookAndFeelClassName()
        // For the System look and feel: getSystemLookAndFeelClassName()
        lookAndFeel = UIManager.getSystemLookAndFeelClassName();
        try {
            UIManager.setLookAndFeel(lookAndFeel);
        } catch (Exception e) {
            System.err.println("Exception when initializing the look and feel.");
            System.err.println(e);
        }
    }

    private static void createAndShowGUI() {
        // set the look and feel
        initLookAndFeel();

        //Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(true);

        //Create and set up the window.
        MainExeFrame frame = new MainExeFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
