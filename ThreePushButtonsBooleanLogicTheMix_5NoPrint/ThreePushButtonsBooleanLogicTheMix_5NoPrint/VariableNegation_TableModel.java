

import javax.swing.table.AbstractTableModel;


public class VariableNegation_TableModel extends AbstractTableModel {
    private VariableNegationControl control;
    private String[] outputValues;
    private Object[][] dados;

    public VariableNegation_TableModel(VariableNegationControl c)  {
    	super();
        control = c;
        control.prepareArrayResult();
        createdados();
        
    }

    public int getColumnCount() {
       return (control.getNumberOfColumnsInArrayResult()+2);

    }
    public int getRowCount() {
        return control.getNumberOfLinesInArrayResult();
    }
    public String getColumnName(int col) {
        return control.getVariableName(col);
    }
   
    public void createdados(){  
    	Object[][] d = new Object [getRowCount()][getColumnCount()];
    	for (int row = 0; row < getRowCount(); row++) {
    		for (int col = 0; col < getColumnCount(); col++) {
    	        if (col==0){
    	            d[row][col]=(String)control.getCellValue(row, col);
    	        }else{
    	        	if (col==getColumnCount()-1)
    	        		d[row][col]= Boolean.FALSE;
    	        	 else
    	    	        	d[row][col]= (Integer.parseInt(control.getCellValue(row, col))==1) ? "X" : "";
    	        }
    	       
    		}
		}

    	dados=d;
     }

    
    
    public Object getValueAt(int row, int col) {
    	return dados[row][col];
    }
    
//    public Object getValueAt(int row, int col) {
//        String s = control.getCellValue(row, col);
//        if (col==0)
//            return s;
//        if (col==getColumnCount()-1)
//            return Boolean.FALSE;
//        else
//            return (Integer.parseInt(s)==1) ? "X" : "";
//    }

    
    

    public Class getColumnClass(int col) {
        Class c = null;
        try {
            if (col==getColumnCount()-1)
                c = Class.forName("java.lang.Boolean");
            else
                c = Class.forName("java.lang.String");
        } catch (java.lang.ClassNotFoundException e) {
            System.err.println("ClassNotFooundException when looking for the Class of String or Boolean!");
        }               
        return c;
    }
    
    
    public boolean isCellEditable(int row, int col) {
        if (col==(getColumnCount()-1))
            return true;
        else
            return false;
    }
    
    public void setValueAt(Object value, int row, int col) {
    	if(col==getColumnCount()-1){
    		dados[row][col]=(Boolean) value;
    	}
    	
    	
    }

}
