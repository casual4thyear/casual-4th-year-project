import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.UIManager;

public class MainExeFrame extends JFrame implements ActionListener {
    private JPanel thePanel;
    final static String TruthTableConst = "Truth Table";
    final static String DNFExpressionConst = "DNF Expression";
    final static String VariableNegationConst = "Variable Negation";  
    private JButton ttb,vnb,dnfb; 
    private TruthTableExeFrame ttef;
    private DNFExeFrame dnfef;
    private VariableNegationExeFrame vnef;   
   	private MainExeGUITruthTable megtt;
	private MainExeGUIDNF megdnf;
	private MainExeGUIVariableNegation megvn;
   
    public MainExeFrame() {
        super("Main Exe Frame");
        thePanel = new JPanel();
        thePanel.setLayout(new BoxLayout(thePanel,BoxLayout.X_AXIS));
        thePanel.setBorder(BorderFactory.createTitledBorder("Boolean Logic Computations:"));
        getContentPane().add(thePanel);
        ttb=new JButton(TruthTableConst);
    	dnfb=new JButton(DNFExpressionConst);
    	vnb=new JButton(VariableNegationConst);
    	ttb.addActionListener(this);
    	vnb.addActionListener(this);
    	dnfb.addActionListener(this);
    	thePanel.add(ttb);
    	thePanel.add(dnfb);
    	thePanel.add(vnb);    	
   }
    
    public void actionPerformed(ActionEvent ev)
 	{
 	  if (ev.getActionCommand()==TruthTableConst)
 	    	megtt= new MainExeGUITruthTable();
 	  else if (ev.getActionCommand()==DNFExpressionConst)
 			megdnf= new MainExeGUIDNF();
 	  else if (ev.getActionCommand()==VariableNegationConst)
			megvn=new MainExeGUIVariableNegation();
 	}

    class MainExeGUITruthTable {
    	public MainExeGUITruthTable(){
			trigger();
		}
		private  void initLookAndFeel() {
	        String lookAndFeel = null;
	        // For the Java look and feel: getCrossPlatformLookAndFeelClassName()
	        // For the System look and feel: getSystemLookAndFeelClassName()
	        lookAndFeel = UIManager.getSystemLookAndFeelClassName();
	        try {
	            UIManager.setLookAndFeel(lookAndFeel);
	        } catch (Exception e) {
	            System.err.println("Exception when initializing the look and feel.");
	            System.err.println(e);
	        }
	    }
	    private  void createAndShowGUI() {
	        // set the look and feel
	        initLookAndFeel();
	        //Make sure we have nice window decorations.
	        JFrame.setDefaultLookAndFeelDecorated(true);
	        //Create and set up the window.
	        ttef= new TruthTableExeFrame();
	        //Display the window.
	        ttef.pack();
	        ttef.setVisible(true);
	    }	
	    private  void trigger() {
	        javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                createAndShowGUI();
	            }
	        });
	    }
	}

	class MainExeGUIDNF {
		public MainExeGUIDNF(){
			trigger();
		}
		private  void initLookAndFeel() {
	        String lookAndFeel = null;
	        // For the Java look and feel: getCrossPlatformLookAndFeelClassName()
	        // For the System look and feel: getSystemLookAndFeelClassName()
	        lookAndFeel = UIManager.getSystemLookAndFeelClassName();
	        try {
	            UIManager.setLookAndFeel(lookAndFeel);
	        } catch (Exception e) {
	            System.err.println("Exception when initializing the look and feel.");
	            System.err.println(e);
	        }
	    }

	    private  void createAndShowGUI() {
	        // set the look and feel
	        initLookAndFeel();
	        //Make sure we have nice window decorations.
	        JFrame.setDefaultLookAndFeelDecorated(true);
	        //Create and set up the window.
	        dnfef= new DNFExeFrame();
	        //Display the window.
	        dnfef.pack();
	        dnfef.setVisible(true);
	    }

	    private  void trigger() {
	        javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                createAndShowGUI();
	            }
	        });
	    }
	}
	
	class MainExeGUIVariableNegation {
		public MainExeGUIVariableNegation(){
			trigger();
		}
		private  void initLookAndFeel() {
	        String lookAndFeel = null;
	        // For the Java look and feel: getCrossPlatformLookAndFeelClassName()
	        // For the System look and feel: getSystemLookAndFeelClassName()
	        lookAndFeel = UIManager.getSystemLookAndFeelClassName();
	        try {
	            UIManager.setLookAndFeel(lookAndFeel);
	        } catch (Exception e) {
	            System.err.println("Exception when initializing the look and feel.");
	            System.err.println(e);
	        }
	    }
	    private  void createAndShowGUI() {
	        // set the look and feel
	        initLookAndFeel();
	        //Make sure we have nice window decorations.
	        JFrame.setDefaultLookAndFeelDecorated(true);
	        //Create and set up the window.
	        vnef= new VariableNegationExeFrame();
	        //Display the window.
	        vnef.pack();
	        vnef.setVisible(true);
	    }	
	    private  void trigger () {
	        javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                createAndShowGUI();
	            }
	        });
	    }
	}
}

