

import junit.framework.*;

public class VariableNegationAllTest {
	public static Test suite(){
		
		
		
		TestSuite suite = new TestSuite();      
					
		suite.addTest(new VariableNegationControlTest("testVariableNegation1"));	
		suite.addTest(new VariableNegationControlTest("testVariableNegation2"));
		suite.addTest(new VariableNegationControlTest("testPrepareArrayResult"));
		
		
		
		return suite;
		
	}

}
