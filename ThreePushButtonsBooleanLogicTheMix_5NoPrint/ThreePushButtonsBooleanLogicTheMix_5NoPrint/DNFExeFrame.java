

import javax.swing.*;
import javax.swing.event.*;

public class DNFExeFrame extends JFrame{
    private JPanel truthTablePanel;
    public DNFExeFrame() {
        super("DNF Exe Frame");
        truthTablePanel = new JPanel();
        DNFComponent d = new DNFComponent();
        d.setParentFrame(this);
        truthTablePanel.add("DNF Expression", d.getComponent());
        getContentPane().add(truthTablePanel);
    }
}

