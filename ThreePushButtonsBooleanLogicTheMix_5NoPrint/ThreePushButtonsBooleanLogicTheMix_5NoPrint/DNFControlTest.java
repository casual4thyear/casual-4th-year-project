

import junit.framework.*;

public class DNFControlTest extends TestCase {
	DNFControl dfn;
	public DNFControlTest(String a){
		super(a);
	}	
	
	private void createDNF(String logExpression){
		try {
			dfn = new DNFControl(logExpression);			
			
		}	catch (Exception e) {
		 	System.out.println(e);
		}
	}
	
	public void testSize (){
		String logExpression = "(a or b) and c";
		createDNF(logExpression);
		int expected=3;
		assertEquals(expected, dfn.size());
	}
	
	public void testGet1 (){
		String logExpression = "(a or b) and c";
		createDNF(logExpression);
		String expected[]={"NOT(a) AND b AND c","a AND NOT(b) AND c","a AND b AND c"};
		String actual[]={dfn.get(0),dfn.get(1)};
		assertEquals(expected[0], actual[0]);
		assertEquals(expected[1], actual[1]);
	}
	
	public void testGet2 (){
		String logExpression = "(a AND b) XOR c";
		createDNF(logExpression);
		String expected[]={"NOT(a) AND NOT(b) AND c","NOT(a) AND b AND c","a AND NOT(b) AND c","a AND b AND NOT(c)"};
		String actual[]={dfn.get(0),dfn.get(1),dfn.get(2),dfn.get(3)};
		assertEquals(expected[0], actual[0]);
		assertEquals(expected[1], actual[1]);
		assertEquals(expected[2], actual[2]);
		assertEquals(expected[3], actual[3]);
	}
	
}