

import junit.framework.*;

public class TurthTableAllTest {
	public static Test suite(){
				
		TestSuite suite = new TestSuite();      
				
		suite.addTest(new TruthTableTest("testTruthTable"));
		suite.addTest(new TruthTableTest("testVariableNamesTTC"));
		suite.addTest(new TruthTableTest("testColumnHeadersTTC"));
		suite.addTest(new TruthTableTest("testGetFormattedTruthTableTTC1"));
		suite.addTest(new TruthTableTest("testGetFormattedTruthTableTTC2"));
		suite.addTest(new TruthTableTest("testGetFormattedTruthTableTTC"));
		return suite;
		
	}

}
