

import javax.swing.*;
import javax.swing.event.*;

public class TruthTableExeFrame extends JFrame{

    private JPanel thePanel;

    public TruthTableExeFrame() {
        super("Truth Table Exe Frame");
        thePanel = new JPanel();
        TruthTableComponent c = new TruthTableComponent();
        c.setParentFrame(this);
        thePanel.add("Truth Table", c.getComponent());        
        getContentPane().add(thePanel);
    }
}

