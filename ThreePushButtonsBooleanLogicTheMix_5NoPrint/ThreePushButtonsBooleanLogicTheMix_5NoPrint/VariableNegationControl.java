

import java.util.Enumeration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class VariableNegationControl {
    private ArrayList allVariableNames;
    private ArrayList allCubes;
    private ArrayList uniqueTruePoints;
    //uniqueTruePoints.get(i) is an ArrayList instance containing the unique
    //true points for cube allCubes.get(i)
    private ArrayList nearFalsePoints;
    //nearFalsePoints.get(i) is a HashMap instance, whose keys are the variable
    //names in AllVariableNames and values are instances of ArrayList containing
    //the near false points of allCubes.get(i) when negating the (key) variable.
    //i.e., the near false points (an instance of ArrayList) of the second cube
    //(term in the DNF form) when negating variable X are in
    //nearFalsePoints.get(1).get("X")

    private ArrayList arrayResultLabels;
    private ArrayList arrayResultLabelsCaption;
    private int numberOfColumnsInArrayResult;
    private int numberOfLinesInArrayResult;
    private int[][] arrayResult;
    private String[] arrayResultFirstColumn;
    private int[] arrayResultTestCase;

    public VariableNegationControl(Enumeration DNFTerms)throws ParseException, BinaryExpressionSolverException{
        computeCubes(DNFTerms);

        computeUniqueTruePoints();

        computeNearFalsePoints();

        //printAllCubes();

/*        prepareArrayResult();
        System.out.println("columns: "+numberOfColumnsInArrayResult);
        System.out.println("lines: "+numberOfLinesInArrayResult);
        for (int i=0; i<numberOfLinesInArrayResult; i++) {
            System.out.print(arrayResultFirstColumn[i]+" ... ");
            for (int j=0; j<numberOfColumnsInArrayResult; j++)
                System.out.print(arrayResult[i][j]+" ");
            System.out.println();
        }
        for (int i=0; i<arrayResultLabels.size(); i++) {
            System.out.println(arrayResultLabels.get(i));
        }
*/
    }

    private void computeNearFalsePoints() {
        int i, j, k;
        Cube current, negate, variant, other;
        Enumeration enu;
        HashMap hm;
        ArrayList list;
        String key;
        boolean test;

        nearFalsePoints = new ArrayList();
        for(i=0; i<allCubes.size(); i++) {
            hm = new HashMap();
            nearFalsePoints.add(hm);
            current = (Cube)allCubes.get(i);
            //negating each variable in the cube and looking for variants
            //that negate the whole function
            for(j=0; j<current.theCube.length; j++) {
                //the key and value for the hashmap
                key = (String)allVariableNames.get(j);
                list = new ArrayList();
                hm.put(key,list);
                if(current.theCube[j]==-1)
                    continue;
                //negate the jth element of the cube (in a new cube)
                negate = new Cube(current.theCube, current.varNames);
                negate.theCube[j]= (negate.theCube[j]==0) ? 1 : 0;
                //look at each variant of the negation cube
               enu = negate.computeCubeVariants();
                while(enu.hasMoreElements()) {
                    variant = (Cube)enu.nextElement();
                    test = true;
                    for(k=0; k<allCubes.size(); k++) {
                        if (k==i)
                            continue;
                        other = (Cube)allCubes.get(k);
                        if (other.evaluateCube(variant.theCube)) {
                            test=false;
                            break;
                        }
                    }
                    if (test) {
                        list.add(variant);
                    }
                }
            }
        }
    }

    private void computeUniqueTruePoints() {
        int i, j;
        Cube current, other, variant;
       Enumeration enu;
        boolean test;

        uniqueTruePoints = new ArrayList();
        ArrayList l;

        for(i=0; i<allCubes.size(); i++) {
            //for all the cubes
            current = (Cube)allCubes.get(i);
            //create a new data structure to hold its unique true points
            l = new ArrayList();
            //add the newly created data structure to the ArrayList containing
            //all the unique true points.
            uniqueTruePoints.add(l);
            //look at all the variants for the current cube
           enu = current.computeCubeVariants();
            while(enu.hasMoreElements()) {
                variant = (Cube)enu.nextElement();
                test = true;
                //If one of the other cubes evaluates to true for the variant,
                //then the variant is not a unique true point.
                //Otherwise (all the other cubes evaluate to false with this
                //variant), it is a unique true point.
                for(j=0; j<allCubes.size(); j++) {
                    if (j==i)
                        continue;
                    other = (Cube)allCubes.get(j);
                    if (other.evaluateCube(variant.theCube)) {
                        test=false;
                        break;
                    }
                }
                if (test) {
                    //none of the other cubes evaluates to true with this variant
                    //we have a unique true point
                    l.add(variant);
                }
            }
        }
    }

    public String getRawResults() {
        String res = "";
       Enumeration enu;
        Cube current, variant, c;
        int[] v;
        int i,j,k;
        ArrayList l;
        HashMap h;

        res+="All cubes:"+"\n";
        for(i=0; i<allCubes.size(); i++) {
            current = (Cube)allCubes.get(i);
            res+="Cubes "+(i+1)+": "+current+" \""+current.stringValue()+"\""+"\n";

            //printing variants
            res+="\tVariants:"+"\n";
           enu = current.computeCubeVariants();
            while(enu.hasMoreElements()) {
                variant = (Cube)enu.nextElement();
                res+="\t\tvariant: "+variant+" ("+variant.intValue()+")"+"\n";
            }

            //printing unique true points
            res+="\tUnique true points:"+"\n";
            l = (ArrayList)uniqueTruePoints.get(i);
            for(j=0; j<l.size();j++) {
                c = (Cube)l.get(j);
                res+="\t\t"+c+" ("+c.intValue()+")"+"\n";
            }

            //printing near false points
            res+="\tNear False Points:"+"\n";
            h = (HashMap)nearFalsePoints.get(i);
            for(j=0; j<allVariableNames.size(); j++) {
                l = (ArrayList)h.get(allVariableNames.get(j));
                if (l.size()==0)
                    continue;
                res+="\t\tWhen negating: "+allVariableNames.get(j)+"\n";
                for(k=0; k<l.size(); k++) {
                    c = (Cube)l.get(k);
                    res+="\t\t\t"+c+" ("+c.intValue()+")"+"\n";
                }
            }
        }
        return res;
    }

    private void printAllCubes() {
        System.out.println(getRawResults());
    }

    private void computeCubes(Enumeration DNFTerms)throws ParseException, BinaryExpressionSolverException{
        String term;
        TruthTableControl ttc;
        ArrayList allTtc;

        allTtc = new ArrayList();
        allVariableNames = new ArrayList();
        while(DNFTerms.hasMoreElements()) {
            term = (String)DNFTerms.nextElement();
            ttc = new TruthTableControl(term);
            allTtc.add(ttc);
            allVariableNames.addAll(Arrays.asList(ttc.variableNames()));
        }

        //System.out.println(allVarNames);
        //Remove duplicates in allVarNames
        for(int i=allVariableNames.size()-1; i>=0; i--) {
            if (allVariableNames.indexOf(allVariableNames.get(i))<i)
                allVariableNames.remove(i);
        }
        //System.out.println(allVarNames);

        allCubes = new ArrayList();
        for(int i=0; i<allTtc.size(); i++) {
            ttc = (TruthTableControl)allTtc.get(i);
            allCubes.add(new Cube(ttc, allVariableNames));
        }
    }


    public void prepareArrayResult() {
        int i, j, k, column;
        String s;
        HashMap h;
        ArrayList l;
        Cube current, c;

        //When counting the number of columns in the Array result:
        //First, account for the number of unique true points (one unique true
        // point per cube - thus numberOfColumnsInArrayResult++ in for loop).
        //Then account for the number of near false points.

        for (numberOfColumnsInArrayResult=0, i=0; i<allCubes.size(); i++, numberOfColumnsInArrayResult++) {
            h = (HashMap)nearFalsePoints.get(i);
            for(j=0; j<allVariableNames.size(); j++) {
                l = (ArrayList)h.get(allVariableNames.get(j));
                if (l.size()==0)
                    continue;
                numberOfColumnsInArrayResult++;
            }
        }
        numberOfLinesInArrayResult = (int)Math.pow(2, allVariableNames.size());

        arrayResultFirstColumn = new String[numberOfLinesInArrayResult];

        arrayResult = new int[numberOfLinesInArrayResult][numberOfColumnsInArrayResult];
        arrayResultFirstColumn = new String[numberOfLinesInArrayResult];

        for (i=0; i<numberOfLinesInArrayResult; i++) {
            arrayResultFirstColumn[i] = getVariantString(i);
            for (j=0; j<numberOfColumnsInArrayResult; j++)
                arrayResult[i][j] = 0;
        }

        arrayResultLabels = new ArrayList();
        arrayResultLabelsCaption = new ArrayList();
        arrayResultLabels.add("");
        column = 0;
        for (i=0; i<allCubes.size(); i++) {
            //look at unique true points for the cube
            current = (Cube)allCubes.get(i);
            arrayResultLabels.add("("+(column+1)+")");
            arrayResultLabelsCaption.add("("+(column+1)+") = UTP for \""+current.stringValue()+"\"");
            l = (ArrayList)uniqueTruePoints.get(i);
            for (j=0; j<l.size(); j++) {
                c = (Cube)l.get(j);
                arrayResult[c.intValue()][column] = 1;
            }
            column++;
            //look at near false points for the cube
            h = (HashMap)nearFalsePoints.get(i);
            for(j=0; j<allVariableNames.size(); j++) {
                l = (ArrayList)h.get(allVariableNames.get(j));
                if (l.size()==0)
                    continue;
                arrayResultLabels.add("("+(column+1)+")");
                arrayResultLabelsCaption.add("("+(column+1)+") = NFP for \""+current.stringValue()+"\" negating \""+allVariableNames.get(j)+"\"");
                for(k=0; k<l.size(); k++) {
                    c = (Cube)l.get(k);
                    arrayResult[c.intValue()][column] = 1;
                }
                column++;
            }
        }
        arrayResultLabels.add("Test Case");
        arrayResultLabels.add("Your Test Case");

        determineTestCase();
    }

    public ArrayList getLabelsCaption() {
        return arrayResultLabelsCaption;
    }

    private String getVariantString(int line) {
        int i, mask=1;
        String ret = "";
        for (i=0; i<allVariableNames.size(); i++) {
            ret += ( (line>>>(allVariableNames.size()-i-1))&mask ) + " ";
        }
        return ret + " (" + (line) + ")";
    }

    private void determineTestCase() {
        int[] covered;
        int i, j, sum, line, value, maxLine, maxValue;
        boolean done;

        covered = new int[numberOfColumnsInArrayResult];
        for (i=0; i<covered.length; i++)
            covered[i] = 0;
        arrayResultTestCase = new int[numberOfLinesInArrayResult];
        for (i=0; i<arrayResultTestCase.length; i++)
            arrayResultTestCase[i] = 0;

        done = false;
        while(!done) {
            maxValue = 0;
            maxLine = 0;
            //determine the line that allows the coverage of the maximum number
            //of columns remaining to be covered.
            for(line=0; line<arrayResultTestCase.length; line++) {
                if (arrayResultTestCase[line]==1) //the line has already been selected
                    continue;
                //determine the value of the line
                //how many remaining columns it covers
                value = 0;
                for(j=0; j<covered.length; j++) {
                    if (covered[j]==1)
                        continue;
                    if (arrayResult[line][j]==1)
                        value++;
                }
                if (value>maxValue)
                    maxLine = line;
            }
            //we have found a line
            arrayResultTestCase[maxLine]=1;
            for(j=0; j<covered.length; j++)
                if (arrayResult[maxLine][j]==1)
                    covered[j] = 1;

            //Have we covered all the columns? i.e., are we done?
            sum=0;
            for(i=0; i<covered.length;i++)
                sum+=covered[i];
            if (sum==covered.length)
                done = true;
        }
    }

    public int getNumberOfColumnsInArrayResult() {
        return numberOfColumnsInArrayResult + 1; //plus one for a column of checkboxes
    }

    public int getNumberOfLinesInArrayResult() {
        return numberOfLinesInArrayResult;
    }

    public String getVariableName(int col) {
        return (String)arrayResultLabels.get(col);
    }

    public String getCellValue(int l, int c) {
        if (c==0)
            return arrayResultFirstColumn[l];
        if (c==numberOfColumnsInArrayResult+1)
            return Integer.toString(arrayResultTestCase[l]);
        if (c==numberOfColumnsInArrayResult+2)
            return "";
        return Integer.toString(arrayResult[l][c-1]);
    }

    //start - modified 19/aug - bruna
    
    public ArrayList getAllVariableNames(){
    	
    	return allVariableNames;
    }
    public ArrayList getAllCubes(){
    	
    	return allCubes;
    }
    public ArrayList getUniqueTruePoints(){
    	
    	return uniqueTruePoints;
    }
    public ArrayList getNearFalsePoints(){
    	
    	return nearFalsePoints;
    }
    
    // end
}