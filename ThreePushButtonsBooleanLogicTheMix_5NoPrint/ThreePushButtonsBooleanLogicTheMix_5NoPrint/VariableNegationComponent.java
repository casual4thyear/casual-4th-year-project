

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import java.util.Vector;
import java.util.Enumeration;
import java.util.ArrayList;



public class VariableNegationComponent implements ActionListener, DocumentListener {
    private static int jTextFieldSize = 15;

    private JPanel mainPane;
    private JFrame parentFrame;
    private JPanel productTermsPanel;
    private JTextArea resultArea;
    private JButton computeButton;
    private JButton showTestCaseButton;
    private Dimension dim = new Dimension(300,25);

    private Vector listOfJTextField;

    private String addProductTermButtonString = "Add Product Term";
    private String computeButtonString = "Compute Var. Neg.";
    private String showTestCaseButtonString = "Show Test Cases";

    VariableNegationControl control;

    public VariableNegationComponent() {
        listOfJTextField = new Vector();
        addWidgets();
    }

    public JComponent getComponent() {
        return mainPane;
    }

    public void setParentFrame(JFrame f) {
        parentFrame = f;
        //parentFrame.getRootPane().setDefaultButton(computeButton);
    }

    private void addWidgets() {
        JTextField jtf;

        productTermsPanel = new JPanel();
        productTermsPanel.setLayout(new BoxLayout(productTermsPanel, BoxLayout.Y_AXIS));
        productTermsPanel.setBorder(BorderFactory.createTitledBorder("Product Terms"));
        jtf = createTextField();
        productTermsPanel.add(jtf);
        listOfJTextField.add(jtf);
        jtf = createTextField();
        productTermsPanel.add(jtf);
        listOfJTextField.add(jtf);
        jtf = createTextField();
        productTermsPanel.add(jtf);
        listOfJTextField.add(jtf);

        JButton addProductTermButton = new JButton(addProductTermButtonString);
        addProductTermButton.addActionListener(this);
        addProductTermButton.setActionCommand(addProductTermButtonString);

        computeButton = new JButton(computeButtonString);
        computeButton.addActionListener(this);
        computeButton.setActionCommand(computeButtonString);
        computeButton.setEnabled(false);

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.X_AXIS));
        inputPanel.add(productTermsPanel);
        inputPanel.add(addProductTermButton);
        inputPanel.add(computeButton);

        JPanel textOutputPanel = new JPanel();
        textOutputPanel.setLayout(new BoxLayout(textOutputPanel,BoxLayout.X_AXIS));
        textOutputPanel.setBorder(BorderFactory.createTitledBorder("Variable Negation Results"));
        resultArea = new JTextArea(10,25);
        resultArea.setEditable(false);
        resultArea.setMaximumSize(new Dimension(300,200));
        textOutputPanel.add(new JScrollPane(resultArea));
        textOutputPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        showTestCaseButton = new JButton(showTestCaseButtonString);
        showTestCaseButton.addActionListener(this);
        showTestCaseButton.setActionCommand(showTestCaseButtonString);
        showTestCaseButton.setEnabled(false);

        JPanel outputPanel = new JPanel();
        outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.X_AXIS));
        outputPanel.add(textOutputPanel);
        outputPanel.add(showTestCaseButton);

        mainPane = new JPanel();
        mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
        mainPane.add(inputPanel);
        mainPane.add(outputPanel);
    }

    private JTextField createTextField() {
        JTextField tf;
        tf = new JTextField(jTextFieldSize);
        //tf.setMinimumSize(dim);
        //tf.setPreferredSize(dim);
        tf.setMaximumSize(dim);
        tf.getDocument().addDocumentListener(this);
        //tf.setActionCommand("");
        return tf;
    }

    public void actionPerformed(ActionEvent evt) {
        int p, k;

        if (evt.getActionCommand().equals(addProductTermButtonString)) {
        	JTextField jtf = createTextField();
            productTermsPanel.add(jtf);
            listOfJTextField.add(jtf);
            parentFrame.pack();
            computeButton.setEnabled(false);
            showTestCaseButton.setEnabled(false);
            return;
        }
        if (evt.getActionCommand().equals(computeButtonString)) {
            JTextField jtf;
            Vector productTerms = new Vector();
            Enumeration enu = listOfJTextField.elements();
           	int it=0;
            while(enu.hasMoreElements()) {
                jtf = (JTextField)enu.nextElement();
                if (jtf.getText().length()==0)
                    continue;
                productTerms.add(jtf.getText());
                 	it++;
                 	

            }


            try {
                control = new VariableNegationControl(productTerms.elements());
                resultArea.setText(control.getRawResults());
            } catch (Exception e) {
                JOptionPane.showMessageDialog(parentFrame, e.getMessage(), "Parsing Error", JOptionPane.ERROR_MESSAGE);
            }
            computeButton.setEnabled(false);
            showTestCaseButton.setEnabled(true);
            return;
        }
        if (evt.getActionCommand().equals(showTestCaseButtonString)) {
         
        	VariableNegationResultsTable vnTable = new VariableNegationResultsTable(control,"Variable Negation Results: Table"); // changes by b 15/09
          vnTable.setVisible(true);
          vnTable.requestFocus();
        	computeButton.setEnabled(false);
            showTestCaseButton.setEnabled(false);
            return;
 
            
            
//        	control.prepareArrayResult();
//            JPanel jp1 = new JPanel();
//            JPanel jp2 = new JPanel();
//            ArrayList captions = control.getLabelsCaption();
//            for (k=0; k<captions.size(); k++)
//                if (k<captions.size()/2)
//                    jp1.add(new JLabel((String)captions.get(k)));
//                else
//                    jp2.add(new JLabel((String)captions.get(k)));
//            JTable resultTable = new JTable(new VariableNegation_TableModel(control));
//
//
//            for (p=1; p<resultTable.getColumnCount()-2; p++)
//                resultTable.getColumnModel().getColumn(p).setMaxWidth(50);
//            resultTable.getColumnModel().getColumn(p).setMaxWidth(70);
//
//
//            JDialog jd = new JDialog(parentFrame, "Variable Negation Results: Table");
//            jd.getContentPane().setLayout(new BoxLayout(jd.getContentPane(), BoxLayout.Y_AXIS));
//            jd.getContentPane().add(jp1);
//            jd.getContentPane().add(jp2);
//            jd.getContentPane().add(new JScrollPane(resultTable));
//            jd.pack();
//            jd.setVisible(true);
        	

        }
        System.err.println("Unknown ActionEvent in VariableNegationComponent: ");
        System.err.println(evt);
    }

    // listeners to detect changes in the input text field and enable the compute button
    public void insertUpdate(DocumentEvent e) {
        theDocumentListener(e);
    }
    public void removeUpdate(DocumentEvent e) {
        theDocumentListener(e);
    }
    public void changedUpdate(DocumentEvent e) {
        theDocumentListener(e);
    }
    private void theDocumentListener(DocumentEvent e) {
        Enumeration enu;
        boolean test;
        JTextField jtf;

        test = false;
        enu = listOfJTextField.elements();
        while(enu.hasMoreElements()) {
            jtf = (JTextField)enu.nextElement();
            if (jtf.getText().length()>0) {
                test = true;
                break;
            }
        }
        if (!test)
            computeButton.setEnabled(false);
        else
            computeButton.setEnabled(true);
    }

}
