

import java.io.StringReader;

public class TruthTableControl
{
	//Variables
	private int [][] truthTable;
	private String[]  variable_Names;
	private String logicalExpression;
	private LogicalExpressionParser parser;
	
	//Constructor - Takes the logic expression as input
	public TruthTableControl(String expr) throws ParseException, BinaryExpressionSolverException{
		logicalExpression = expr;
		parser = new LogicalExpressionParser(new StringReader(logicalExpression));
		parser.Logical_Expression();
		truthTable = new int [getVariantsNumber()][1+getVariableNumber()];
		variable_Names = new String[1+getVariableNumber()];
		buildTruthTable();
		}
	
	//Returns an array of the truth table values
	protected int[][] getTruthTable(){
		return truthTable;
	}
	
	//Returns a String Array containing the names of variables or columns of the truth table.
	public String[] variableNames(){
        String[] varNames = new String[getVariableNumber()];
        for(int i=0; i<varNames.length; i++)
            varNames[i] = getVariableName(i);
        return varNames;
	}
	
	public String[] columnHeaders(){
		 String[] headers = new String[getVariableNumber()+1];
		
		for(int y=0; y<=getVariableNumber(); y++)
			headers[y] = getVariableName(y);
		return headers;
	}
	
	//Returns number of Rows
	public int getVariantsNumber(){
	return (int)(Math.pow(2, getVariableNumber()));
	}
	
	//Returns the number of Columns
	public int getVariableNumber(){
	  return parser.theSetOfBooleanVariables.numberOfVariables();
	}
	
	//Builds the truth table array
	private void buildTruthTable()throws BinaryExpressionSolverException{
		for(int i=0; i<getVariantsNumber(); i++){
			fillTableRow(i);
		}
	}
	
	//Fills the specified row of the truth table with values
	private void fillTableRow(int row)throws BinaryExpressionSolverException{
		BinaryExpressionSolver exprSolver;
		String binaryExpression;
		int columns= getVariableNumber();

		parser.theSetOfBooleanVariables.setVariablesToValue(row);
		binaryExpression = parser.theSetOfBooleanVariables.replaceValuesInExpr(logicalExpression);
		exprSolver = new BinaryExpressionSolver(new StringReader(binaryExpression));
		
		for(int j=0; j<=columns; j++){
			if(j==columns){
				truthTable[row][j] = exprSolver.solve();
				}
			else
				truthTable[row][j] = parser.theSetOfBooleanVariables.getVariable(j).getValue();
		} 
	} 
	
	
	//Returns the variable located at the specified location in the variable_Names array
    private String getVariableName(int index){
    	if(index==getVariableNumber())
    		return logicalExpression;
    	else
    	 return parser.theSetOfBooleanVariables.getVariable(index).getName();
    }
	
    //Returns the function values in the truth table
    protected int[] getFunctionValues(){
        int[] funcVal = new int[getVariantsNumber()];
        for(int i=0; i<funcVal.length; i++) {
            funcVal[i] = getTruthTableCellValue(i,getVariableNumber());
        }
        return funcVal;
    }
    
    //Returns the value at the specified location in the truth table array containing values
    public int getTruthTableCellValue(int row, int col){
    	int [][] table =  getTruthTable();
    	return table[row][col];
    }
    
    //Returns a String array of the truth table content taking into
    //consideration the output option
    public String[][] getFormattedTruthTable(int outputOption){
    	String[][] tableContent;
    	tableContent = new String[getVariantsNumber()][(getVariableNumber()+1)];
    	int x;
    	for(int row=0; row<getVariantsNumber(); row++){
    		for(int col=0; col<(getVariableNumber()+1); col++){
    			x = getTruthTableCellValue(row, col);
    			tableContent[row][col]= fillTableCell(outputOption,x);
    			//System.out.println(row+"\t"+col+"\t"+x);
    		}
    	}
    	return tableContent;
    }

	//Returns the value to be put into the cell of the table 
    //considering the chosen output option
	private String fillTableCell(int outputOption, int value){
	switch(outputOption){
	 case  1: if(value==1)return "true"; else return "false";
	case  2: if(value==1)return "T"; else return "F";
	default: return ""+value;
	}
   }
}

