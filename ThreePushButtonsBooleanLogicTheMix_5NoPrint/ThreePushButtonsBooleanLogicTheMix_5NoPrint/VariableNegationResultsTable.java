

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import java.util.Vector;
import java.util.Enumeration;
import java.util.ArrayList;


public class VariableNegationResultsTable extends JFrame implements ActionListener{
	
	// window opens when showTestCaseButton is pressed
	
	VariableNegationControl control;
	VariableNegation_TableModel tableModel; 
    JPanel jp1 = new JPanel();
    JPanel jp2 = new JPanel();
    JPanel jp3 = new JPanel();
    JButton mytestcaseButton = new JButton("Is my test good?");
    JLabel answerLabel = new JLabel("Answer Here");
	int p, k;
	

	

//	public VariableNegationResultsTable(String s) { //this part is used to run only this window
		public VariableNegationResultsTable(VariableNegationControl c, String s) { //comment this to run only this window
		super(s);		//comment this to run only this window
		control=c;		//comment this to run only this window
		
	//	callVNC();// //this part is used to run only this window
	
		control.prepareArrayResult();
		createcaptionPanels();
		tableModel = new VariableNegation_TableModel(control);
	    JTable resultTable = new JTable(tableModel);
	    for (p=1; p<resultTable.getColumnCount()-2; p++)
	        resultTable.getColumnModel().getColumn(p).setMaxWidth(50);
	    resultTable.getColumnModel().getColumn(p).setMaxWidth(70);
		
		
		
		
		jp3.add(mytestcaseButton);
		jp3.add(answerLabel);
		mytestcaseButton.addActionListener(this);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		getContentPane().add(jp1);
		getContentPane().add(jp2);
		getContentPane().add(new JScrollPane(resultTable));
		getContentPane().add(jp3);
	    pack();
	    
	   // setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //this part is used to run only this window
	   // setVisible(true);  //this part is used to run only this window


	}

	public void createcaptionPanels(){
		ArrayList captions = control.getLabelsCaption();

		
	    for (k=0; k<captions.size(); k++)
	        if (k<captions.size()/2)
	            jp1.add(new JLabel((String)captions.get(k)));
	        else
	            jp2.add(new JLabel((String)captions.get(k)));
	}


	public void actionPerformed(ActionEvent evt) {	//implements button to check if the test marked on checkboxes is good
		CheckTestCase check;
		int lastColumn = tableModel.getColumnCount()-1;
		int lines = tableModel.getRowCount();
		Boolean[] selectedTestCase = new Boolean[lines];
		
		
		if (evt.getActionCommand().equals("Is my test good?")){
				for (int j = 0; j <lines; j++) {
				selectedTestCase[j] = (Boolean) tableModel.getValueAt(j, lastColumn);

			}
				
			check = new CheckTestCase(tableModel);
			if (check.isGoodTest()){
				answerLabel.setText("That's a good choice!");
			}
			else {
				answerLabel.setText("Not really. Try again.");
			}
			
			
			
			
			
		}
		
	}
	
	////////////////		this part is used to run only this window
//	private void callVNC(){
//		Vector productTerms = new Vector(2,5);
//		productTerms.add("a and b and not c");
//		productTerms.add("a and d");
//		try {
//			control = new VariableNegationControl(productTerms.elements());			
//			
//		}	catch (Exception e) {
//		 	System.out.println(e);
//			}
//	}
//	
//
//    public static void main(String[] args) {
//        javax.swing.SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//                VariableNegationResultsTable gui = new VariableNegationResultsTable("HOLA");
//            }
//        });
//    }
	/////////////
}