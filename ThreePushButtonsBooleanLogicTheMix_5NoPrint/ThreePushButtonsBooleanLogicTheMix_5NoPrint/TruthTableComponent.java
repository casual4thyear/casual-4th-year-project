
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;


public class TruthTableComponent implements ActionListener, DocumentListener {
    private JTextField inputExpression;
    private JButton computeButton;
    private JPanel outputPanel;
    private JPanel mainPane;
    private JFrame parentFrame;
    //Action commands for buttons
    private String[] outputOptions = { "0,1    ", "false,true", "F,T    " };
    private String computeCmd = "Compute Truth Table";
    private int currentOutputOption = 0;

    public TruthTableComponent() {
        mainPane = new JPanel();
        mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
        addWidgets();
        mainPane.setVisible(true);
    }

    public JComponent getComponent() {
        return mainPane;
    }

    public void setParentFrame(JFrame f) {
        parentFrame = f;
        parentFrame.getRootPane().setDefaultButton(computeButton);
    }

    private void addWidgets() {
        //Create the input text area
        JPanel textInputPanel = new JPanel();
        textInputPanel.setLayout(new BoxLayout(textInputPanel,BoxLayout.X_AXIS));
        textInputPanel.setBorder(BorderFactory.createTitledBorder("Enter the boolean function below:"));
        inputExpression = new JTextField(20);
        inputExpression.setMaximumSize(new Dimension(300,25));
        inputExpression.getDocument().addDocumentListener(this);
        textInputPanel.add(inputExpression);
        textInputPanel.setMaximumSize(new Dimension(305,50));
        textInputPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        //Create check box buttons for the ouput format
        JRadioButton zeroOne = new JRadioButton(outputOptions[0]);
        zeroOne.setSelected(true);
        zeroOne.addActionListener(this);
        zeroOne.setActionCommand(outputOptions[0]);
        JRadioButton falseTrue = new JRadioButton(outputOptions[1]);
        falseTrue.addActionListener(this);
        falseTrue.setActionCommand(outputOptions[1]);
        JRadioButton fT = new JRadioButton(outputOptions[2]);
        fT.addActionListener(this);
        fT.setActionCommand(outputOptions[2]);
        ButtonGroup group = new ButtonGroup();
        group.add(zeroOne); group.add(falseTrue); group.add(fT);
        JPanel radioPanel = new JPanel(new GridLayout(1,3));
        radioPanel.setBorder(BorderFactory.createTitledBorder("Output format?"));
        radioPanel.add(zeroOne); radioPanel.add(falseTrue); radioPanel.add(fT);
        radioPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        radioPanel.setMaximumSize(new Dimension(305,50));

        //Create a panel with these input elements: lPane
        JPanel lPane = new JPanel();
        lPane.setLayout(new BoxLayout(lPane,BoxLayout.PAGE_AXIS));
        lPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        lPane.add(textInputPanel);
        lPane.add(radioPanel);

        //Create the "Compute" button
        computeButton = new JButton(computeCmd);
        computeButton.addActionListener(this);
        computeButton.setEnabled(false);
        computeButton.setActionCommand(computeCmd);

        //Create an panel with all input elements: inputPane
        JPanel inputPane = new JPanel();
        inputPane.setLayout(new BoxLayout(inputPane, BoxLayout.X_AXIS));
        inputPane.add(lPane);
        inputPane.add(computeButton);
        //inputPane.setAlignmentX(Component.LEFT_ALIGNMENT);

        //Create an output panel to store the result
        outputPanel = new JPanel();
        outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.Y_AXIS));

        //Add everything to the mainPane
        mainPane.add(inputPane);
        mainPane.add(outputPanel);
    }

    public void actionPerformed(ActionEvent evt) {
        int i;      
        if (evt.getActionCommand().equals(computeCmd)) {
        	/*
        	 * The following two line of code were added in order to track execution
        	 * August -4- 2015
        	 */
        
        	if (outputPanel.getComponentCount()!=0)
                //we remove the old truth table and create a new one
                outputPanel.removeAll();
            try {
              	TruthTableControl t = new TruthTableControl(inputExpression.getText());
                JTable resultTable = new JTable(t.getFormattedTruthTable(currentOutputOption),t.columnHeaders());
                resultTable.setPreferredScrollableViewportSize(new Dimension(100,300));
                
                outputPanel.add(new JLabel("Truth table"));
                outputPanel.add(new JScrollPane(resultTable));
                parentFrame.pack();
                parentFrame.validate();
                computeButton.setEnabled(false); //diseable the compute button
            } catch (Exception e) {
                computeButton.setEnabled(false);
                JOptionPane.showMessageDialog(parentFrame, e.getMessage(), "Parsing Error", JOptionPane.ERROR_MESSAGE);
            }
          } else { // one of the radio buttons
        	  for(i=0; i<outputOptions.length; i++)
                if (evt.getActionCommand().equals(outputOptions[i]))
                    break;
            currentOutputOption = i;
            checkComputeButtonEnable();
        }
    }

    // listeners to detect changes in the input text field and enable the compute button
    public void insertUpdate(DocumentEvent e) {
        theDocumentListener(e);
    }
    public void removeUpdate(DocumentEvent e) {
        theDocumentListener(e);
    }
    public void changedUpdate(DocumentEvent e) {
        theDocumentListener(e);
    }
    private void theDocumentListener(DocumentEvent e) {
        checkComputeButtonEnable();
    }

    private void checkComputeButtonEnable() {
        if (inputExpression.getText().length()==0)
            computeButton.setEnabled(false);
        else
            computeButton.setEnabled(true);
    }

}
