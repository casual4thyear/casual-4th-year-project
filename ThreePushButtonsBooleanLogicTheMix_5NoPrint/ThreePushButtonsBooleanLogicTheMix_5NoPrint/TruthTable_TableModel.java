



public class TruthTable_TableModel{
    private TruthTableControl control;
    private int outputOption;

    //Constructor takes as input the Logical Expression and the output option
    public TruthTable_TableModel(String s, int outputOption)throws ParseException, BinaryExpressionSolverException {
        this.outputOption= outputOption;
        control = new TruthTableControl(s);
    }

    
    //Returns a String array of the truth table content taking into
    //consideration the output option
    public String[][] getFormattedTruthTable(){
    	String[][] tableContent;
    	tableContent = new String[getColumnCount()][getRowCount()];
    	int x;
    	for(int row=0; row<=getRowCount(); row++){
    		for(int col=0; col<=getRowCount(); col++){
    			x = control.getTruthTableCellValue(row, col);
    			tableContent[row][col]= fillTableCell(x);
    		}
    	}
    	return tableContent;
    }
    
    //Returns the number of columns in the truth table
    private int getColumnCount() {
        return (control.getVariableNumber()+1);
    }
    
    //Returns the number of rows in the truth table
    private int getRowCount(){
        return control.getVariantsNumber();
    }
    
    //Returns an array of Column names
    public String[] getColumnNames(){
    	return control.variableNames();
    }

	//Returns the value to be put into the cell of the table 
    //considering the choosen output option
	private String fillTableCell(int value){
	switch(outputOption){
	 case  1: if(value==1)return "true"; else return "false";
	case  2: if(value==1)return "T"; else return "F";
	default: return ""+value;
	}
   }
}
