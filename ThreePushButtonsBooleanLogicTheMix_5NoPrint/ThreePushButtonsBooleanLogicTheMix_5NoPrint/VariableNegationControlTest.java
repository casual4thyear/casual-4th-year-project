
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import junit.framework.*;

public class VariableNegationControlTest extends TestCase {
	VariableNegationControl vn;
	
	public VariableNegationControlTest(String a){
		super(a);
	}
	
	private void createVariableNegation(Vector productTerms){
		try {
			vn = new VariableNegationControl(productTerms.elements());			
			
		}	catch (Exception e) {
		 	System.out.println(e);
			}
	}
	
	public void testVariableNegation1(){
		Vector productTerms = new Vector(2,5);
		ArrayList cubes, nearFalsePoints, uniqueTruePoints, allVariables;
		HashMap hashMap=new HashMap();
		int i,j,size;
		//String[] expectedCubes, expectedAllVariables, expectedUniqueTruePoints, expectedNearFalsePoints;
		
		//adding product terms 
		productTerms.add("a and b and not c");
		productTerms.add("a and d");
		createVariableNegation(productTerms);
		
		// 		####	CUBES	####
		
		String[] expectedCubes={"1 1 0 -1 ", "1 -1 -1 1 "};//need to put a space in the end of each string - cube.toStrint puts " "
		cubes = vn.getAllCubes();
		
		size=cubes.size();
		for(i=0;i<size;i++){
			assertEquals(expectedCubes[i], cubes.get(i).toString());
		}
		
		// 		####	UNIQUE TRUE POINTS	####		
		
		String[] expectedTruePoints={"[1 1 0 0 ]", "[1 0 0 1 , 1 0 1 1 , 1 1 1 1 ]"};
		uniqueTruePoints=vn.getUniqueTruePoints();
		
		size=uniqueTruePoints.size();
		for(i=0;i<size;i++){
			assertEquals(expectedTruePoints[i], uniqueTruePoints.get(i).toString());
		}
		
		// 		####	ALL VARIABLES	####
		allVariables=vn.getAllVariableNames();
		String[] expectedAllVariables={"a", "b", "c", "d"};
		for(i=0;i<allVariables.size();i++){
			assertEquals(expectedAllVariables[i], allVariables.get(i));
		}
		
		// 		####	NEAR FALSE POINTS	####
		
		//HashMap expectedNFPHash[] = new HashMap[4];
		nearFalsePoints=vn.getNearFalsePoints();
		String[][] expectedNearFalsePoints=
		{{"[0 1 0 0 , 0 1 0 1 ]", "[0 0 0 1 , 0 0 1 1 , 0 1 0 1 , 0 1 1 1 ]"} //a
		,{"[1 0 0 0 ]", "[]"} //b
		,{"[1 1 1 0 ]", "[]"} //c
		,{"[]", "[1 0 0 0 , 1 0 1 0 , 1 1 1 0 ]"}}; //d
		
		for(i=0;i<size;i++){
			hashMap = (HashMap) nearFalsePoints.get(i);
			for(j=0; j<allVariables.size();j++){

				assertEquals("For Variable "+allVariables.get(j)+" :",expectedNearFalsePoints[j][i],hashMap.get(allVariables.get(j)).toString());
				//System.out.println("hashMap for "+allVariables.get(j)+": " +hashMap.get(allVariables.get(j)));
			}
		}
		
		

/* 			############ PRINTING THE RESULTS #############
 * 
 * 		
		
		cubes = vn.getAllCubes();
		size=cubes.size();
		System.out.println("Cubes: " + cubes);	

		uniqueTruePoints = vn.getUniqueTruePoints();
		System.out.println("UniqueTruePoints: " + uniqueTruePoints);	

		allVariables=vn.getAllVariableNames();
		System.out.println("Variables: " + allVariables);
		
		nearFalsePoints = vn.getNearFalsePoints();
		for(i=0;i<size;i++){
			hashMap = (HashMap) nearFalsePoints.get(i);
			for(int j=0; j<allVariables.size();j++){
				System.out.println("hashMap for "+allVariables.get(j)+": " +hashMap.get(allVariables.get(j)));
			}
			System.out.println("hashMap: " +hashMap.values());
		}
		
*/		
		
	}
	
		
	public void testVariableNegation2(){
		Vector productTerms = new Vector(4,5);
	
		ArrayList cubes, nearFalsePoints, uniqueTruePoints, allVariables;
		HashMap hashMap=new HashMap();
		int i,j,size;
		//String[] expectedCubes, expectedAllVariables, expectedUniqueTruePoints, expectedNearFalsePoints;
		
		//adding product terms 
		productTerms.add("x and not y and z");
		productTerms.add("y and w");
		productTerms.add("not x and w");
		productTerms.add("y and not z");
		
		createVariableNegation(productTerms);
		
		// 		####	CUBES	####
		
		String[] expectedCubes={"1 0 1 -1 ", 
				"-1 1 -1 1 ",
				"0 -1 -1 1 ",
				"-1 1 0 -1 "};//need to put a space in the end of each string - cube.toStrint puts " "
		cubes = vn.getAllCubes();
		
		size=cubes.size();
		for(i=0;i<size;i++){
			assertEquals(expectedCubes[i], cubes.get(i).toString());
		}
		
		// 		####	UNIQUE TRUE POINTS	####		
		
		String[] expectedTruePoints={"[1 0 1 0 , 1 0 1 1 ]", 
				"[1 1 1 1 ]", 
				"[0 0 0 1 , 0 0 1 1 ]", 
				"[0 1 0 0 , 1 1 0 0 ]"};
		uniqueTruePoints=vn.getUniqueTruePoints();
		
		size=uniqueTruePoints.size();
		for(i=0;i<size;i++){
			assertEquals(expectedTruePoints[i], uniqueTruePoints.get(i).toString());
		}
		
		// 		####	ALL VARIABLES	####
		allVariables=vn.getAllVariableNames();
		String[] expectedAllVariables={"x", "y", "z", "w"};
		for(i=0;i<allVariables.size();i++){
			assertEquals(expectedAllVariables[i], allVariables.get(i));
		}
		
		// 		####	NEAR FALSE POINTS	####
		
		//HashMap expectedNFPHash[] = new HashMap[4];
		nearFalsePoints=vn.getNearFalsePoints();
		String[][] expectedNearFalsePoints=
		{{"[0 0 1 0 ]", "[]", "[1 0 0 1 ]", "[]"} //x
		,{"[1 1 1 0 ]", "[1 0 0 1 ]", "[]", "[0 0 0 0 , 1 0 0 0 , 1 0 0 1 ]"} //y
		,{"[1 0 0 0 , 1 0 0 1 ]", "[]", "[]", "[0 1 1 0 , 1 1 1 0 ]"} //z
		,{"[]", "[0 1 1 0 , 1 1 1 0 ]", "[0 0 0 0 , 0 0 1 0 , 0 1 1 0 ]", "[]"}}; //w
		
		for(i=0;i<size;i++){
			hashMap = (HashMap) nearFalsePoints.get(i);
			for(j=0; j<allVariables.size();j++){

				assertEquals("For Variable "+allVariables.get(j)+" :",expectedNearFalsePoints[j][i],hashMap.get(allVariables.get(j)).toString());
				//System.out.println("hashMap for "+allVariables.get(j)+": " +hashMap.get(allVariables.get(j)));
			}
		}
			
		
	}
	
	
	
	public void testPrepareArrayResult(){
		Vector productTerms = new Vector(4,5);
		

		int i,j,size;
		
		//adding product terms 
		productTerms.add("x and not y and z");
		productTerms.add("y and w");
		createVariableNegation(productTerms);
	
		vn.prepareArrayResult();
		
		int expNumberOfColumns = 8; //number of candidates = 6 + col0 + col Test Case
		int expNumberOfLines = 16;	// 2^number of variables	
		assertEquals(expNumberOfColumns, vn.getNumberOfColumnsInArrayResult());
		assertEquals(expNumberOfLines, vn.getNumberOfLinesInArrayResult());
		
		String expLabelsCaption = 
				"[(1) = UTP for \"x and not(y) and z\", " +
				"(2) = NFP for \"x and not(y) and z\" negating \"x\", " +
				"(3) = NFP for \"x and not(y) and z\" negating \"y\", " +
				"(4) = NFP for \"x and not(y) and z\" negating \"z\", " +
				"(5) = UTP for \"y and w\", " +
				"(6) = NFP for \"y and w\" negating \"y\", " +
				"(7) = NFP for \"y and w\" negating \"w\"]";
		assertEquals(expLabelsCaption, vn.getLabelsCaption().toString());

		// first line
		String[] expVariableName={"","(1)","(2)","(3)","(4)","(5)","(6)","(7)","Test Case", "Your Test Case"};	
		for(i=0;i<10;i++){
			assertEquals(expVariableName[i], vn.getVariableName(i));
		}
		
		//table values, excludes last 2 columns = TestCase found + Empty space for "your test"
		String[][] expCellValue={
		{"0 0 0 0  (0)","0","0","0","0","0","0","0"},
		{"0 0 0 1  (1)","0","0","0","0","0","1","0"},
		{"0 0 1 0  (2)","0","1","0","0","0","0","0"},
		{"0 0 1 1  (3)","0","1","0","0","0","1","0"},
		{"0 1 0 0  (4)","0","0","0","0","0","0","1"},
		{"0 1 0 1  (5)","0","0","0","0","1","0","0"},
		{"0 1 1 0  (6)","0","0","0","0","0","0","1"},
		{"0 1 1 1  (7)","0","0","0","0","1","0","0"},
		{"1 0 0 0  (8)","0","0","0","1","0","0","0"},
		{"1 0 0 1  (9)","0","0","0","1","0","1","0"},
		{"1 0 1 0  (10)","1","0","0","0","0","0","0"},
		{"1 0 1 1  (11)","1","0","0","0","0","0","0"},
		{"1 1 0 0  (12)","0","0","0","0","0","0","1"},
		{"1 1 0 1  (13)","0","0","0","0","1","0","0"},
		{"1 1 1 0  (14)","0","0","1","0","0","0","1"},
		{"1 1 1 1  (15)","0","0","0","0","1","0","0"},
		};
		
		//test if the table showing results is ok
		for ( i = 0; i < expNumberOfLines; i++) {
			for ( j = 0; j < expNumberOfColumns; j++) {
				assertEquals("Cell ["+ i+","+ j+"]",expCellValue[i][j],vn.getCellValue(i, j));				
			}
		}
		
		//test if testcase column covers all tests candidates
		int[] cover= new int[expNumberOfColumns-2];
		for ( j = 0; j < expNumberOfColumns-2; j++) {
			cover[j]=0;				//init variable with 0
		}
		String b="0";
		for ( i = 0; i < expNumberOfLines; i++) {
			b=vn.getCellValue(i, expNumberOfColumns); //gets the cell value at the column test case
			if(Integer.parseInt(b)==1){
				for ( j = 1; j < expNumberOfColumns-1; j++) {
					b=vn.getCellValue(i, j);		//get which candidate uses that variant
					if(Integer.parseInt(b)==1){		
						cover[j-1]=1;
					}
				}
			}	
			
		}
		for ( j = 0; j < expNumberOfColumns-2; j++) {
			assertEquals("line "+j+": ",1,cover[j]);		
		}
	
		
	}
	
	
	
	
	
	
}