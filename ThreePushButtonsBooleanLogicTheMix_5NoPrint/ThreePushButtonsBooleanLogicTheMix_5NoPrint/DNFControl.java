

import java.util.ArrayList;




public class DNFControl{
	TruthTableControl table;
	ArrayList DNFTerms;
	String[] HeaderColumns;
	int [][] tableContent;
	
 public DNFControl (String expr)throws ParseException, BinaryExpressionSolverException{
	 TruthTableControl table = new TruthTableControl(expr);
	 tableContent = table.getTruthTable();
	// HeaderColumns = table.variableNames();
	 HeaderColumns = table.columnHeaders();
	 DNFTerms = new ArrayList();
	 fillTerms();
 }
 
 public int size(){
	 return DNFTerms.size();
 }
 
 public String get(int i){
	 return DNFTerms.get(i).toString();
 }
 
// private void fillTerms(){
//	  int rows = tableContent.length;
//	  int columns = HeaderColumns.length;
//	  String term;
//	  for(int i=0; i<rows; i++)
//	  {  
//		  term="";
//		  if(tableContent[i][columns-1]==1)
//		  {
//			 for(int j=0; j<(columns-1); j++)	//doesn't get the last variable..
//			 {
//			  if(tableContent[i][j]==0){
//				  term = term + " not ("+ HeaderColumns[j]+")";
//			  }
//			  else{
//				  term = term + " "+ HeaderColumns[j];
//			  }
//			  if(j != (columns-2)) {term += " and ";}
//			 }
//			 DNFTerms.add(term);
//			}
//	     }
//}
 
 
 private void fillTerms(){
	  int rows = tableContent.length;
	  int columns = HeaderColumns.length;
	  String term;
	  for(int i=0; i<rows; i++)
	  {  
		  term="";
		  if(tableContent[i][columns-1]==1)
		  {
			 for(int j=0; j<(columns-1); j++)
			 {
			  if(tableContent[i][j]==0){
				  term = term + "NOT("+ HeaderColumns[j]+")";
			  }
			  else{
				  term = term + HeaderColumns[j];
			  }
			  if(j != (columns-2)) {term += " AND ";}
			 }
			 DNFTerms.add(term);
			}
	     }
} 
 
 
 
 
}
